<%--
  Created by IntelliJ IDEA.
  User: cjnora
  Date: 2018/9/28
  Time: 上午12:48
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html>
<head>
    <title>Oh!Oh!Shop|Shopping Cart</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap-4/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="bootstrap-4/css/bootstrap.min.css">
    <!--CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.1/css/all.css" integrity="sha384-5sAR7xN1Nv6T6+dT2mhtzEpVJvfS3NScPQTrOxhwjIuvcA67KV2R5Jz6kr4abQsz" crossorigin="anonymous">



    <link href="/open-iconic/font/css/open-iconic-bootstrap.css" rel="stylesheet">
    <link rel="stylesheet" href="main.css">
</head>

<body>
<div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="index.jsp">Oh!Oh!Shop</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index.jsp">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/item">MyCart</a><span class="sr-only">(current)</span>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Brands</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="marioBrand.jsp">Mario</a>
                        <a class="dropdown-item" href="pokemonBrand.jsp">Pokemon</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="help.jsp">Help</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <form action="order" method="get" id="orderForm" name="orderForm" role="form" >
                        <input type="hidden" id="searchAction" name="searchAction" value="searchOrderByCurrentUser">
                        <%
                            if (session.getAttribute("user") != null) {
                                out.print("<a class=\"login-link\" href=\"#\" onclick=\"document.getElementById('orderForm').submit();\" >  Order</a>");
                            } else {
                                out.print("<a class=\"login-link\" href=\"login.jsp\">Login</a>");
                            }
                        %>
                    </form>
                </li>
            </ul>
        </div>
    </nav>

    <%--<!--Search Form -->--%>
    <form action="item" method="get" id="seachItemForm" role="form">
    <input type="hidden" id="searchAction2" name="searchAction" value="searchByName">
    <div class="form-group col-xs-5">
    <input type="text" name="itemName" id="itemName" class="form-control" placeholder="Type the Name of the item"/>
    </div>
    <button type="submit" class="btn btn-info">
    <span class="glyphicon glyphicon-search"></span> Search
    </button>
    <br>
    <br>
    </form>



    <!--Employees List-->
    <c:if test="${not empty message}">
    <div class="alert alert-success">
    ${message}
    </div>
    </c:if>
    <form action="item" method="post" id="itemForm" role="form" >
    <input type="hidden" id="idItem" name="idItem">
    <input type="hidden" id="action" name="action">
        <input type="hidden" id="clas" name="clas">
        <input type="hidden" id="name" name="name">
        <input type="hidden" id="quantity" name="quantity">
        <input type="hidden" id="price" name="price">
        <c:if test="${not empty undoList}">
            <Button type="submit" id="undo" value="undo" id="undo" class="btn btn-md coolStuffBtn2"  onclick="document.getElementById('action').value = 'undo';document.getElementById('itemForm').submit();"><i class="fas fa-undo"></i></Button>
        </c:if>
    <c:choose>
    <c:when test="${not empty itemList}">
    <table  class="table table-striped">
    <thead>
    <tr>
    <td>#</td>
    <td>Type</td>
    <td>Name</td>
        <td>Quantity</td>
    <td>Price</td>
    <td></td>
    </tr>
    </thead>
        <c:set var="totalPrice" value="${0}"/>
    <c:forEach var="item" items="${itemList}">
    <c:set var="classSucess" value=""/>
    <c:if test ="${idItem == item.id}">
    <c:set var="classSucess" value="info"/>
    </c:if>
        <c:set var="itemPrice" value="${item.price * item.quantity}"/>
        <c:set var="totalPrice" value="${totalPrice + itemPrice}"/>
    <tr class="${classSucess}">
    <td>
        <c:if test="${item.clas == 'Mario'}">
            <c:forEach var="i" items="${marioList}">
                <c:if test="${i.name == item.name}">
                    <img src="img/MARIO/${i.id}.jpg" style="width: 50px;height: 50px;"/>
                </c:if>
            </c:forEach>
        </c:if>
        <c:if test="${item.clas == 'Pokemon'}">
            <c:forEach var="i" items="${pokemonList}">
                <c:if test="${i.name == item.name}">
                    <img src="img/POKEMON/${i.id}.jpg" style="width: 50px;height: 50px;"/>
                </c:if>
            </c:forEach>
        </c:if>
        <c:if test="${item.clas == 'Charmander'}">
            <c:forEach var="i" items="${charmanderList}">
                <c:if test="${i.name == item.name}">
                    <img src="img/Charmander/${i.id}.jpg" style="width: 50px;height: 50px;"/>
                </c:if>
            </c:forEach>
        </c:if>
        <c:if test="${item.clas == 'Luigi'}">
            <c:forEach var="i" items="${luigiList}">
                <c:if test="${i.name == item.name}">
                    <img src="img/Luigi/${i.id}.jpg" style="width: 50px;height: 50px;"/>
                </c:if>
            </c:forEach>
        </c:if>
    </td>
    <td>${item.clas}</td>
    <td>${item.name}</td>
        <td>
            <c:if test="${(item.quantity - 1) >= 1}">
                <Input type="submit" id="minus" value="-" class="btn btn-default btn-sm" onclick="document.getElementById('action').value = 'edit';document.getElementById('idItem').value = '${item.id}';document.getElementById('clas').value = '${item.clas}';document.getElementById('name').value = '${item.name}';document.getElementById('quantity').value = '${item.quantity - 1}';document.getElementById('price').value = '${item.price}';document.getElementById('itemForm').submit();"/>
            </c:if>
            <c:if test="${item.quantity == 1}">
                <Input type="submit" id="minus" value="-" class="btn btn-default btn-sm" onclick="document.getElementById('action').value = 'edit';document.getElementById('idItem').value = '${item.id}';document.getElementById('clas').value = '${item.clas}';document.getElementById('name').value = '${item.name}';document.getElementById('quantity').value = '${item.quantity}';document.getElementById('price').value = '${item.price}';document.getElementById('itemForm').submit();"/>
            </c:if>
                ${item.quantity}
            <c:if test="${item.clas == 'Mario'}">
                <c:forEach var="i" items="${marioList}" >
                    <c:if test="${i.name == item.name}">
                        <c:if test="${i.quantity gt item.quantity}">
                            <Input type="submit" id="plus" value="+" class="btn btn-default btn-sm" onclick="document.getElementById('action').value = 'edit';document.getElementById('idItem').value = '${item.id}';document.getElementById('clas').value = '${item.clas}';document.getElementById('name').value = '${item.name}';document.getElementById('quantity').value = '${item.quantity + 1}';document.getElementById('price').value = '${item.price}';document.getElementById('itemForm').submit();"/>
                        </c:if>
                    </c:if>
                </c:forEach>
            </c:if>
            <c:if test="${item.clas == 'Pokemon'}">
                <c:forEach var="i" items="${pokemonList}" >
                    <c:if test="${i.name == item.name}">
                        <c:if test="${i.quantity gt item.quantity}">
                            <Input type="submit" id="plus" value="+" class="btn btn-default btn-sm" onclick="document.getElementById('action').value = 'edit';document.getElementById('idItem').value = '${item.id}';document.getElementById('clas').value = '${item.clas}';document.getElementById('name').value = '${item.name}';document.getElementById('quantity').value = '${item.quantity + 1}';document.getElementById('price').value = '${item.price}';document.getElementById('itemForm').submit();"/>
                        </c:if>
                    </c:if>
                </c:forEach>
            </c:if>
            <c:if test="${item.clas == 'Charmander'}">
                <c:forEach var="i" items="${charmanderList}" >
                    <c:if test="${i.name == item.name}">
                        <c:if test="${i.quantity gt item.quantity}">
                            <Input type="submit" id="plus" value="+" class="btn btn-default btn-sm" onclick="document.getElementById('action').value = 'edit';document.getElementById('idItem').value = '${item.id}';document.getElementById('clas').value = '${item.clas}';document.getElementById('name').value = '${item.name}';document.getElementById('quantity').value = '${item.quantity + 1}';document.getElementById('price').value = '${item.price}';document.getElementById('itemForm').submit();"/>
                        </c:if>
                    </c:if>
                </c:forEach>
            </c:if>
            <c:if test="${item.clas == 'Luigi'}">
                <c:forEach var="i" items="${luigiList}" >
                    <c:if test="${i.name == item.name}">
                        <c:if test="${i.quantity gt item.quantity}">
                            <Input type="submit" id="plus" value="+" class="btn btn-default btn-sm" onclick="document.getElementById('action').value = 'edit';document.getElementById('idItem').value = '${item.id}';document.getElementById('clas').value = '${item.clas}';document.getElementById('name').value = '${item.name}';document.getElementById('quantity').value = '${item.quantity + 1}';document.getElementById('price').value = '${item.price}';document.getElementById('itemForm').submit();"/>
                        </c:if>
                    </c:if>
                </c:forEach>
            </c:if>
        </td>
    <td>${item.price}</td>

    <td><a href="#" id="remove"
    onclick="document.getElementById('action').value = 'remove';document.getElementById('idItem').value = '${item.id}';

    document.getElementById('itemForm').submit();">
    remove
    </a>

    </td>
    </tr>
    </c:forEach>
    </table>

        <label>Total Price:$${totalPrice}</label>
        <br>
        <br>
        <label>
            &nbsp;Discount Policy:
            <br>
            &nbsp; 1. If you buy three kinds of items, you will get 5% off:
               <br>
            &nbsp;&nbsp;&nbsp;&nbsp;Your price will be: $${totalPrice} * 95% = $${totalPrice * 0.95}
            <br>
            &nbsp;2. If you buy 100 units of a product, you will get 15% off:
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;ex: Quantities of Mario * Price of Mario * 85%.
            <br>
            &nbsp;3. If you has brought in the last year more than $100K, he gets a 20% off:
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;Your price will be: $${totalPrice} * 80% = $${totalPrice * 0.8}
            <br>
            &nbsp;4. If today is the day of national holidays, you will one product free, when you buy the product two or more.
            <br>
            &nbsp;&nbsp;&nbsp;&nbsp;ex: Your total price - price of a Mario.
        </label>
    </c:when>
    <c:otherwise>
    <br>
    <div class="alert alert-info">
    No item found matching your search criteria
    </div>
    </c:otherwise>
    </c:choose>
    </form>

    <br>
    <div>

        <form action="item" method="post" id="nextForm" role="form" >
            <input type="hidden" id="idItem1" name="idItem">
            <input type="hidden" id="action1" name="action">
            <%
                if(session.getAttribute("user") != null) {
                    if ((long)session.getAttribute("user") == 001) {
                        out.print("<button type=\"submit\" class=\"btn btn-md coolStuffBtn2\" onclick=\"document.getElementById('action1').value = 'goToManMarioPage';document.getElementById('nextForm').submit();\" >Continue Shopping</button>");
                    } else {
                    out.print("<button type=\"submit\" class=\"btn btn-md coolStuffBtn2\" onclick=\"document.getElementById('action1').value = 'goToMarioPage';document.getElementById('nextForm').submit();\" >Continue Shopping</button>");
                    }
                }else {
                    out.print("<button type=\"submit\" class=\"btn btn-md coolStuffBtn2\" onclick=\"document.getElementById('action1').value = 'goToMarioPage';document.getElementById('nextForm').submit();\" >Continue Shopping</button>");

                }
            %>

            <c:set var="checkItemList" value="${itemList}"/>
        <%--<c:if test="${not empty itemList}">--%>


        <%--</c:if>--%>
    </form>
        <br>
        <form action="account" method="post" id="actForm" role="form" >
            <input type="hidden" id="actionAct" name="action">
    <c:if test="${not empty checkItemList}">
            <button type="submit" class="btn  btn-md coolStuffBtn2" onclick="document.getElementById('actionAct').value = 'checkCurrentUser';document.getElementById('actForm').submit();">Next Step</button>
    </c:if>
        </form>

    </div>
</div>

<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="bootstrap-4/js/bootstrap.min.js"></script>

</body>
</html>
