<%--
  Created by IntelliJ IDEA.
  User: cjnora
  Date: 2018/9/28
  Time: 上午12:48
  To change this template use File | Settings | File Templates.
--%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!doctype html>
<html>
<head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap-4/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="bootstrap-4/css/bootstrap.min.css">
    <!--CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="main.css">
</head>

<body class="container">
<div class="col-md-12">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="#">SWAG</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item  ">
                    <a class="nav-link" href="index.jsp">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link active" href="shoppingcart.jsp">MyCart</a><span class="sr-only">(current)</span>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Swag</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="mario.jsp">Mario</a>
                        <a class="dropdown-item" href="pokemon.jsp">Pokemon</a>
                    </div>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <a class="login-link" href="login.jsp">Login</a>
                </li>
            </ul>
        </div>
    </nav>

    <!--Search Form -->
    <form action="item" method="get" id="seachItemForm" role="form">
        <input type="hidden" id="searchAction" name="searchAction" value="searchByName">
        <div class="form-group col-xs-5">
            <input type="text" name="itemName" id="itemName" class="form-control" required="true" placeholder="Type the Name of the item"/>
        </div>
        <button type="submit" class="btn btn-info">
            <span class="glyphicon glyphicon-search"></span> Search
        </button>
        <br>
        <br>
    </form>

    <!--Employees List-->
    <c:if test="${not empty message}">
        <div class="alert alert-success">
                ${message}
        </div>
    </c:if>
    <form action="item" method="post" id="itemForm" role="form" >
        <input type="hidden" id="idItem" name="idItem">
        <input type="hidden" id="action" name="action">
        <c:choose>
            <c:when test="${not empty itemList}">
                <table  class="table table-striped">
                    <thead>
                    <tr>
                        <td>#</td>
                        <td>Type</td>
                        <td>Name</td>
                        <td>Price</td>
                        <td></td>
                    </tr>
                    </thead>
                    <c:forEach var="item" items="${itemList}">
                        <c:set var="classSucess" value=""/>
                        <c:if test ="${idItem == item.id}">
                            <c:set var="classSucess" value="info"/>
                        </c:if>
                        <tr class="${classSucess}">
                            <td>
                                <a href="item?idItem=${item.id}&searchAction=searchById">${item.id}</a>
                            </td>
                            <td>${item.type}</td>
                            <td>${item.name}</td>
                            <td>${item.price}</td>
                            <td><a href="#" id="remove"
                                   onclick="document.getElementById('action').value = 'remove';document.getElementById('idItem').value = '${item.id}';

                                           document.getElementById('itemForm').submit();">
                                <span class="glyphicon glyphicon-trash"> </span>
                            </a>

                            </td>
                        </tr>
                    </c:forEach>
                </table>
            </c:when>
            <c:otherwise>
                <br>
                <div class="alert alert-info">
                    No people found matching your search criteria
                </div>
            </c:otherwise>
        </c:choose>
    </form>
    <form action ="mario.jsp">
        <br></br>
        <button type="submit" class="btn btn-primary  btn-md">New Item</button>
    </form>
</div>
</body>
</html>
<%--<table class="table  table-bordered" >--%>
    <%--<tr>--%>
        <%--<td>--%>

            <%--<div align="center">--%>

                <%--<table  class="table  table-bordered"  style="width:980px;">--%>

                    <%--<tr>--%>
                        <%--<td>品名：</td>--%>
                        <%--<td>單價：</td>--%>
                        <%--<td>數量：</td>--%>
                        <%--<td>小計：</td>--%>
                        <%--<td>刪除：</td>--%>

                    <%--</tr>--%>
                    <%--<tr>--%>
                        <%--<td>商品</td>--%>
                        <%--<td>價格</td>--%>
                        <%--<td>數量</td>--%>
                        <%--<td>小計</td>--%>
                        <%--<td>刪除</td>--%>
                    <%--</tr>--%>


                <%--</table>--%>


            <%--</div>--%>


        <%--</td>--%>
    <%--</tr>--%>
    <%--<tr>--%>
        <%--<td align="center"> <h3><button type="button" class="btn btn-danger btn-sm coolStuffBtn2" data-toggle="modal" data-target=".bscrook-modal-sm">繼續購物</button>&nbsp;&nbsp;&nbsp;<button type="button" class="btn btn-danger btn-sm coolStuffBtn2" data-toggle="modal" data-target=".bscrook-modal-sm">確認</button></h3></td>--%>

    <%--</tr>--%>
<%--</table>--%>