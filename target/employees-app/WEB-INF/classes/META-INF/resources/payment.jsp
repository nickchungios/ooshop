<%--
  Created by IntelliJ IDEA.
  User: cjnora
  Date: 2018/10/7
  Time: 上午3:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
<head>
    <title>Oh!Oh!Shop|Payment</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <style type="text/css">
        @import "main.css";
    </style>

</head>
<body>

<div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="index.jsp">Oh!Oh!Shop</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index.jsp">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/item">MyCart</a><span class="sr-only">(current)</span>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Brands</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="marioBrand.jsp">Mario</a>
                        <a class="dropdown-item" href="pokemonBrand.jsp">Pokemon</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="help.jsp">Help</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <form action="account" method="post" id="logoutForm" name="logoutForm" role="form" >
                        <input type="hidden" id="currentPage" name="currentPage">
                        <input type="hidden" id="action" name="action">
                        <%
                            if (session.getAttribute("user") != null) {
                                out.print("<a class=\"login-link\" href=\"#\" onclick=\"document.getElementById('action').value = 'logout';document.getElementById('currentPage').value = '/index.jsp';document.getElementById('logoutForm').submit();\">Logout</a>");
                            } else {
                                out.print("<a class=\"login-link\" href=\"login.jsp\">Login</a>");
                            }
                        %>
                    </form>
                    <form action="order" method="get" id="orderForm" name="orderForm" role="form" >
                        <input type="hidden" id="searchAction" name="searchAction" value="searchOrderByCurrentUser">
                        <%
                            if (session.getAttribute("user") != null) {
                                out.print("<a class=\"login-link\" href=\"#\" onclick=\"document.getElementById('orderForm').submit();\" >  Order</a>");
                            }
                        %>
                    </form>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container">
        <form action="/order" method="post"  role="form" data-toggle="validator" >
            <c:if test ="${empty action}">
                <c:set var="action" value="add"/>
            </c:if>
            <input type="hidden" id="action" name="action" value="${action}">
            <input type="hidden" id="idOrder" name="idOrder" value="${order.id}">
            <div class="form-row">
                <div class="form-group col-md-6">
                    <label >First name</label>
                    <input name = "firstName" type="text" class="form-control" placeholder="First name" required="required">
                </div>
                <div class="form-group col-md-6">
                    <label >Last name</label>
                    <input name = "lastName" type="text" class="form-control" placeholder="Last name" required="required">
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-8">
                    <label for="inputEmail4">Email</label>
                    <input name = "email" type="email" class="form-control" id="inputEmail4" placeholder="Email" required="required">
                </div>
                <div class="form-group col-md-4">
                    <label for="inputEmail4">Phone Number</label>
                    <input name = "phone" type="tel" class="form-control" id="inputPhone" placeholder="Phone Number" required="required">
                </div>
            </div>

            <div class="form-group">
                <label for="inputAddress">Address</label>
                <input name = "address" type="text" class="form-control" id="inputAddress" placeholder="1234 Main St" required="required">
            </div>
            <div class="form-group">
                <label for="PaymentSelect">Payment</label>
                <select name ="pay" class="form-control" id="PaymentSelect">
                    <option value="COD">COD</option>
                    <option value="ATM">ATM</option>
                    <option value="LinePay">LinePay</option>
                    <option value="WechatPay">WechatPay</option>
                </select>
            </div>
            <div id="ATM">
            </div>


            <button type="submit" class="btn btn-primary">Purchase</button>
        </form>
    </div>
</div>
<script>

    $('#PaymentSelect').on('change', function() {
        if($('#PaymentSelect').val()!="ATM"){
            $("#ATMpay").remove();
        }else{
            $("#ATM").append("            <div id=\"ATMpay\">\n" +
                "                <div class=\"form-group\">\n" +
                "                    <label for=\"cardNumber\">Card Number</label>\n" +
                "                    <input name =\"cardNumber\" maxlength=\"16\" type=\"text\"  class=\"form-control\" id=\"cardNumber\" placeholder=\"Card Number\" required=\"ture\" onkeypress='validate(event)'>\n" +
                "                </div>\n" +
                "                <div class=\"form-row\" >\n" +
                "                    <div class=\"form-group col-md-3\">\n" +
                "                        <label for=\"MonthControlSelect\">Month</label>\n" +
                "                        <select name =\"month\" class=\"form-control\" id=\"MonthControlSelect\">\n" +
                "                            <% for(int i = 1; i < 13; i+=1) { %>\n" +
                "                            <option value=\"<%=i%>\"><%=i%></option>\n" +
                "                            <% } %>\n" +
                "                        </select>\n" +
                "                    </div>\n" +
                "                    <div class=\"form-group col-md-3\">\n" +
                "                        <label for=\"YearControlSelect\">Year</label>\n" +
                "                        <select name =\"year\" class=\"form-control\" id=\"YearControlSelect\">\n" +
                "                            <% for(int i = 2018; i < 2030; i+=1) { %>\n" +
                "                            <option value=\"<%=i%>\"><%=i%></option >\n" +
                "                            <% } %>\n" +
                "                        </select>\n" +
                "                    </div>\n" +
                "                    <div class=\"form-group col-md-6\">\n" +
                "                        <label for=\"inputEmail5\">CYC</label>\n" +
                "                        <input type=\"text\" name =\"cyc\" maxlength=\"3\" class=\"form-control\" id=\"inputEmail5\" placeholder=\"CYC\" required=\"ture\" onkeypress='validate(event)'>\n" +
                "                    </div>\n" +
                "                </div>\n" +
                "            </div>");
        }
    });
    function validate(evt) {
        var theEvent = evt || window.event;

        // Handle paste
        if (theEvent.type === 'paste') {
            key = event.clipboardData.getData('text/plain');
        } else {
            // Handle key press
            var key = theEvent.keyCode || theEvent.which;
            key = String.fromCharCode(key);
        }
        var regex = /[0-9]|\./;
        if( !regex.test(key) ) {
            theEvent.returnValue = false;
            if(theEvent.preventDefault) theEvent.preventDefault();
        }
    }
</script>
</body>
</html>
