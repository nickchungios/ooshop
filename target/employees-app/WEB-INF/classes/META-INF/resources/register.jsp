<%--
  Created by IntelliJ IDEA.
  User: cjnora
  Date: 2018/9/28
  Time: 上午12:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Oh!Oh!Shop|Register</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap-4/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="bootstrap-4/css/bootstrap.min.css">
    <link rel="stylesheet" href="login.css">
</head>

<body class="signin-body bg-dark">
<div class="container signin-container">
    <div class="row">
        <div class="col"></div>
        <div class="col-sm-12 col-md-8">
            <div class="card signin-card">
                <div class="card-body">
                        <a class="navbar-brand text-dark" href="index.jsp">Oh!Oh!Shop</a>
                    <c:if test="${not empty message}">
                        <div class="alert alert-success">
                                ${message}
                        </div>
                    </c:if>
                    <form class="signin-form" action="account" method="post" id="signinForm" role="form" >
                        <c:set var="action" value="create"/>
                        <input type="hidden" id="uid" name="uid" >
                        <input type="hidden" id="action" name="action" value="${action}">
                        <div class="form-group">
                            <input type="email" class="form-control" id="emailInput" name="email" placeholder="Email" required="required">
                        </div>
                        <div class="form-group">
                            <input type="password" class="form-control" id="passwordInput" name="password" placeholder="Password" required="required">
                        </div>
                        <Button type="submit" class="btn signin-btn btn-lg">Sign up</Button>
                        <div class="form-check">
                            <label class="form-check-label">
                                <input class="form-check-input" type="checkbox"> Remember me
                                <a href="#"> Need Help?</a>
                            </label>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <div class="col"></div>
    </div>
</div>





<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<!--  Always Download latest version of Boostrap and add here-->
<script src="bootstrap-4/js/bootstrap.min.js"></script>
</body>

</html>
