<%--
  Created by IntelliJ IDEA.
  User: cjnora
  Date: 2018/11/6
  Time: 6:34 PM
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.net.*" %>
<!DOCTYPE html>
<html lang="en">

<head>
    <title>Oh!Oh!Shop|Sold Out Message</title>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="bootstrap-4/css/bootstrap-grid.min.css">
    <link rel="stylesheet" href="bootstrap-4/css/bootstrap.min.css">
    <!--CSS-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="main.css">
</head>
<body>
<div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="index.jsp">Oh!Oh!Shop</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index.jsp">Home</a><span class="sr-only">(current)</span>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/item">MyCart</a>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Brands</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="marioBrand.jsp">Mario</a>
                        <a class="dropdown-item" href="pokemonBrand.jsp">Pokemon</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="help.jsp">Help</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <form action="account" method="post" id="logoutForm" name="logoutForm" role="form" >
                        <input type="hidden" id="currentPage" name="currentPage">
                        <input type="hidden" id="action" name="action">
                        <%
                            if (session.getAttribute("user") != null) {
                                out.print("<a class=\"login-link\" href=\"#\" onclick=\"document.getElementById('action').value = 'logout';document.getElementById('currentPage').value = '/index.jsp';document.getElementById('logoutForm').submit();\">Logout</a>");
                            } else {
                                out.print("<a class=\"login-link\" href=\"login.jsp\">Login</a>");
                            }
                        %>
                    </form>
                    <form action="order" method="get" id="orderForm" name="orderForm" role="form" >
                        <input type="hidden" id="searchAction" name="searchAction" value="searchOrderByCurrentUser">
                        <%
                            if (session.getAttribute("user") != null) {
                                out.print("<a class=\"login-link\" href=\"#\" onclick=\"document.getElementById('orderForm').submit();\" >  Order</a>");
                            }
                        %>
                    </form>
                </li>
            </ul>
        </div>
    </nav>

    <!--Section One-->
    <div class="sectionLight">
        <div class="row">
            <div class="col-sm-12">
                <div class="jumbotron jumbotron-fluid">
                    <div class="container">
                        <h1 class="display-4">Oh!Oh!Shop</h1>
                        <p class="lead">Sorry! The item has been sold out! Please change quantities of item or delete.</p>
                    </div>
                </div>
            </div>
        </div>

    <!-- jQuery first, then Tether, then Bootstrap JS. -->
    <script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
    <script src="bootstrap-4/js/bootstrap.min.js"></script>
</body>
</html>
