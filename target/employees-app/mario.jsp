
<%--
  Created by IntelliJ IDEA.
  User: cjnora
  Date: 2018/9/29
  Time: 下午5:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
<head>
    <title>Oh!Oh!Shop|Mario</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <style type="text/css">
        @import "main.css";
    </style>

</head>
<body>

<div class="container-fluid">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="index.jsp">Oh!Oh!Shop</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index.jsp">Home</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="/item">MyCart</a>
                </li>
                <li class="nav-item dropdown active">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Brands</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="marioBrand.jsp">Mario</a>
                        <a class="dropdown-item" href="pokemonBrand.jsp">Pokemon</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="help.jsp">Help</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <form action="account" method="post" id="logoutForm" name="logoutForm" role="form" >
                        <input type="hidden" id="currentPage" name="currentPage">
                        <input type="hidden" id="action" name="action">
                        <%
                            if (session.getAttribute("user") != null) {
                                out.print("<a class=\"login-link\" href=\"#\" onclick=\"document.getElementById('action').value = 'logout';document.getElementById('currentPage').value = '/index.jsp';document.getElementById('logoutForm').submit();\">Logout</a>");
                            } else {
                                out.print("<a class=\"login-link\" href=\"login.jsp\">Login</a>");
                            }
                        %>
                    </form>
                    <form action="order" method="get" id="orderForm" name="orderForm" role="form" >
                        <input type="hidden" id="searchAction" name="searchAction" value="searchOrderByCurrentUser">
                        <%
                            if (session.getAttribute("user") != null) {
                                out.print("<a class=\"login-link\" href=\"#\" onclick=\"document.getElementById('orderForm').submit();\" >  Order</a>");
                            }
                        %>
                    </form>
                </li>
            </ul>
        </div>
    </nav>


    <div class = "board">
        <div class="row" >
            <div class="col-md-4">
                <form action="/item" method="post"  role="form" data-toggle="validator" >
                    <c:if test ="${empty action}">
                        <c:set var="action" value="add"/>
                    </c:if>
                    <input type="hidden" id="action" name="action" value="${action}">
                    <input type="hidden" id="idItem" name="idItem" value="${item.id}">
                    <div class="form-group col-xs-4">
                        <c:choose>
                            <c:when test="${not empty marioList}">
                                <c:forEach var="item" items="${marioList}">
                                <c:set var="classSucess" value=""/>
                                <c:if test ="${idItem == item.id}">
                                    <c:set var="classSucess" value="info"/>
                                </c:if>
                                    <c:if test="${item.id == '1'}">
                                        <div><img src="./img/MARIO/1.jpg" alt="Smiley face" height="auto" width="25%"></div>

                                        <label for="clas1" class="control-label col-xs-4">${item.clas}</label><br>
                                        <input type="hidden" name="clas" id="clas1" class="form-control" value="${item.clas}" required="true"/>

                                        <label for="name1" class="control-label col-xs-4">Name : ${item.name}</label><br>
                                        <input type="hidden" name="name" id="name1" class="form-control" value="${item.name}" required="true"/>

                                        <input type="hidden" name="quantity" id="quantity1" class="form-control" value="1" required="true"/>

                                        <label class="control-label col-xs-4">Quantity : ${item.quantity}</label><br>

                                        <label for="price1" class="control-label col-xs-4">Price : ${item.price}</label><br>
                                        <input type="hidden" name="price" id="price1" class="form-control" value="${item.price}" required="true"/>


                                        <br></br>
                                        <c:choose>
                                            <c:when test="${item.quantity == 0}">
                                                <button type="button" class="btn btn-primary  btn-md" data-toggle="modal" data-target="#soldOutModal1">Sold Out!</button>
                                            </c:when>
                                            <c:otherwise>
                                                <button type="submit" class="btn btn-primary  btn-md">Add to cart</button>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </form>
            </div>

            <div class="col-md-4">

                <form action="/item" method="post"  role="form" data-toggle="validator" >
                    <c:if test ="${empty action}">
                        <c:set var="action" value="add"/>
                    </c:if>
                    <input type="hidden" id="action" name="action" value="${action}">
                    <input type="hidden" id="idItem" name="idItem" value="${item.id}">
                    <div class="form-group col-xs-4">
                        <c:choose>
                            <c:when test="${not empty marioList}">
                                <c:forEach var="item" items="${marioList}">
                                    <c:set var="classSucess" value=""/>
                                    <c:if test ="${idItem == item.id}">
                                        <c:set var="classSucess" value="info"/>
                                    </c:if>
                                    <c:if test="${item.id == '2'}">
                                        <div><img src="./img/MARIO/2.jpg" alt="Smiley face" height="auto" width="25%"></div>

                                        <label for="clas1" class="control-label col-xs-4">${item.clas}</label><br>
                                        <input type="hidden" name="clas" id="clas1" class="form-control" value="${item.clas}" required="true"/>

                                        <label for="name1" class="control-label col-xs-4">Name : ${item.name}</label><br>
                                        <input type="hidden" name="name" id="name1" class="form-control" value="${item.name}" required="true"/>

                                        <input type="hidden" name="quantity" id="quantity1" class="form-control" value="1" required="true"/>

                                        <label class="control-label col-xs-4">Quantity : ${item.quantity}</label><br>

                                        <label for="price1" class="control-label col-xs-4">Price : ${item.price}</label><br>
                                        <input type="hidden" name="price" id="price1" class="form-control" value="${item.price}" required="true"/>


                                        <br></br>
                                        <c:choose>
                                            <c:when test="${item.quantity == 0}">
                                                <button type="button" class="btn btn-primary  btn-md" data-toggle="modal" data-target="#soldOutModal2">Sold Out!</button>
                                            </c:when>
                                            <c:otherwise>
                                                <button type="submit" class="btn btn-primary  btn-md">Add to cart</button>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </form>
            </div>


            <div class="col-md-4">
                <form action="/item" method="post"  role="form" data-toggle="validator" >
                    <c:if test ="${empty action}">
                        <c:set var="action" value="add"/>
                    </c:if>
                    <input type="hidden" id="action" name="action" value="${action}">
                    <input type="hidden" id="idItem" name="idItem" value="${item.id}">
                    <div class="form-group col-xs-4">
                        <c:choose>
                            <c:when test="${not empty marioList}">
                                <c:forEach var="item" items="${marioList}">
                                    <c:set var="classSucess" value=""/>
                                    <c:if test ="${idItem == item.id}">
                                        <c:set var="classSucess" value="info"/>
                                    </c:if>
                                    <c:if test="${item.id == '3'}">
                                        <div><img src="./img/MARIO/3.jpg" alt="Smiley face" height="auto" width="25%"></div>

                                        <label for="clas1" class="control-label col-xs-4">${item.clas}</label><br>
                                        <input type="hidden" name="clas" id="clas1" class="form-control" value="${item.clas}" required="true"/>

                                        <label for="name1" class="control-label col-xs-4">Name : ${item.name}</label><br>
                                        <input type="hidden" name="name" id="name1" class="form-control" value="${item.name}" required="true"/>

                                        <input type="hidden" name="quantity" id="quantity1" class="form-control" value="1" required="true"/>

                                        <label class="control-label col-xs-4">Quantity : ${item.quantity}</label><br>

                                        <label for="price1" class="control-label col-xs-4">Price : ${item.price}</label><br>
                                        <input type="hidden" name="price" id="price1" class="form-control" value="${item.price}" required="true"/>


                                        <br></br>
                                        <c:choose>
                                            <c:when test="${item.quantity == 0}">
                                                <button type="button" class="btn btn-primary  btn-md" data-toggle="modal" data-target="#soldOutModal3">Sold Out!</button>
                                            </c:when>
                                            <c:otherwise>
                                                <button type="submit" class="btn btn-primary  btn-md">Add to cart</button>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </form>
            </div>
        </div>


        <div class="row" >
            <div class="col-md-4">

                <form action="/item" method="post"  role="form" data-toggle="validator" >
                    <c:if test ="${empty action}">
                        <c:set var="action" value="add"/>
                    </c:if>
                    <input type="hidden" id="action" name="action" value="${action}">
                    <input type="hidden" id="idItem" name="idItem" value="${item.id}">
                    <div class="form-group col-xs-4">
                        <c:choose>
                            <c:when test="${not empty marioList}">
                                <c:forEach var="item" items="${marioList}">
                                    <c:set var="classSucess" value=""/>
                                    <c:if test ="${idItem == item.id}">
                                        <c:set var="classSucess" value="info"/>
                                    </c:if>
                                    <c:if test="${item.id == '4'}">
                                        <div><img src="./img/MARIO/5.jpg" alt="Smiley face" height="auto" width="25%"></div>

                                        <label for="clas1" class="control-label col-xs-4">${item.clas}</label><br>
                                        <input type="hidden" name="clas" id="clas1" class="form-control" value="${item.clas}" required="true"/>

                                        <label for="name1" class="control-label col-xs-4">Name : ${item.name}</label><br>
                                        <input type="hidden" name="name" id="name1" class="form-control" value="${item.name}" required="true"/>

                                        <input type="hidden" name="quantity" id="quantity1" class="form-control" value="1" required="true"/>

                                        <label class="control-label col-xs-4">Quantity : ${item.quantity}</label><br>

                                        <label for="price1" class="control-label col-xs-4">Price : ${item.price}</label><br>
                                        <input type="hidden" name="price" id="price1" class="form-control" value="${item.price}" required="true"/>


                                        <br></br>
                                        <c:choose>
                                            <c:when test="${item.quantity == 0}">
                                                <button type="button" class="btn btn-primary  btn-md" data-toggle="modal" data-target="#soldOutModal4">Sold Out!</button>
                                            </c:when>
                                            <c:otherwise>
                                                <button type="submit" class="btn btn-primary  btn-md">Add to cart</button>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </form>
            </div>


            <div class="col-md-4">
                <form action="/item" method="post"  role="form" data-toggle="validator" >
                    <c:if test ="${empty action}">
                        <c:set var="action" value="add"/>
                    </c:if>
                    <input type="hidden" id="action" name="action" value="${action}">
                    <input type="hidden" id="idItem" name="idItem" value="${item.id}">
                    <div class="form-group col-xs-4">
                        <c:choose>
                            <c:when test="${not empty marioList}">
                                <c:forEach var="item" items="${marioList}">
                                    <c:set var="classSucess" value=""/>
                                    <c:if test ="${idItem == item.id}">
                                        <c:set var="classSucess" value="info"/>
                                    </c:if>
                                    <c:if test="${item.id == '5'}">
                                        <div><img src="./img/MARIO/6.jpg" alt="Smiley face" height="auto" width="25%"></div>

                                        <label for="clas1" class="control-label col-xs-4">${item.clas}</label><br>
                                        <input type="hidden" name="clas" id="clas1" class="form-control" value="${item.clas}" required="true"/>

                                        <label for="name1" class="control-label col-xs-4">Name : ${item.name}</label><br>
                                        <input type="hidden" name="name" id="name1" class="form-control" value="${item.name}" required="true"/>

                                        <input type="hidden" name="quantity" id="quantity1" class="form-control" value="1" required="true"/>

                                        <label class="control-label col-xs-4">Quantity : ${item.quantity}</label><br>

                                        <label for="price1" class="control-label col-xs-4">Price : ${item.price}</label><br>
                                        <input type="hidden" name="price" id="price1" class="form-control" value="${item.price}" required="true"/>


                                        <br></br>
                                        <c:choose>
                                            <c:when test="${item.quantity == 0}">
                                                <button type="button" class="btn btn-primary  btn-md" data-toggle="modal" data-target="#soldOutModal5">Sold Out!</button>
                                            </c:when>
                                            <c:otherwise>
                                                <button type="submit" class="btn btn-primary  btn-md">Add to cart</button>
                                            </c:otherwise>
                                        </c:choose>
                                    </c:if>
                                </c:forEach>
                            </c:when>
                            <c:otherwise>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </form>
            </div>

        </div>
    </div>

    <!--Sold Out Modal1-->
    <div class="modal fade" id="soldOutModal1" tabindex="-1" role="dialog" aria-labelledby="collStuffLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="contentLabel1">Subscribe When Available</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form form action="/item" method="post"  role="form" data-toggle="validator">
                    <input type="hidden" id="subAction1" name="action" value="addMarioSub">
                    <input type="hidden" id="SubIdItem1" name="idItem" value="1">
                    <div class="modal-body">
                        <div class="container-fluid">
                            <div class="row">
                                <div class="col-sm-12">
                                    <img class="img-fluid"src="./img/MARIO/1.jpg">
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-sm-12">
                                    <div class="modalFormWrapper">

                                        <div class="form-group">
                                            <label for="formName1">What's your name?</label>
                                            <input type="text" class="form-control" id="formName1" name="name" placeholder="Name" required="required">
                                        </div>
                                        <div class="form-group">
                                            <label for="formEmail1">Where should we send your cool content?</label>
                                            <input type="email" class="form-control" id="formEmail1" name="email" placeholder="Email" required="required">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="submit" class="btn btn-secondary">Subscribe</button>
                    </div>
                </form>
        </div>
    </div>
</div>

<!--Sold Out Modal2-->
<div class="modal fade" id="soldOutModal2" tabindex="-1" role="dialog" aria-labelledby="collStuffLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contentLabel2">Subscribe When Available</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form form action="/item" method="post"  role="form" data-toggle="validator">
                <input type="hidden" id="subAction2" name="action" value="addMarioSub">
                <input type="hidden" id="SubIdItem2" name="idItem" value="2">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <img class="img-fluid"src="./img/MARIO/2.jpg">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="modalFormWrapper">

                                    <div class="form-group">
                                        <label for="formName2">What's your name?</label>
                                        <input type="text" class="form-control" id="formName2" name="name" placeholder="Name" required="required">
                                    </div>
                                    <div class="form-group">
                                        <label for="formEmail2">Where should we send your cool content?</label>
                                        <input type="email" class="form-control" id="formEmail2" name="email" placeholder="Email" required="required">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-secondary">Subscribe</button>
                </div>
            </form>
    </div>
</div>
</div>

<!--Sold Out Modal3-->
<div class="modal fade" id="soldOutModal3" tabindex="-1" role="dialog" aria-labelledby="collStuffLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contentLabel3">Subscribe When Available</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form form action="/item" method="post"  role="form" data-toggle="validator">
                <input type="hidden" id="subAction3" name="action" value="addMarioSub">
                <input type="hidden" id="SubIdItem3" name="idItem" value="3">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <img class="img-fluid"src="./img/MARIO/3.jpg">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="modalFormWrapper">

                                    <div class="form-group">
                                        <label for="formName3">What's your name?</label>
                                        <input type="text" class="form-control" id="formName3" name="name" placeholder="Name" required="required">
                                    </div>
                                    <div class="form-group">
                                        <label for="formEmail3">Where should we send your cool content?</label>
                                        <input type="email" class="form-control" id="formEmail3" name="email" placeholder="Email" required="required">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-secondary">Subscribe</button>
                </div>
            </form>
    </div>
</div>
</div>

<!--Sold Out Modal4-->
<div class="modal fade" id="soldOutModal4" tabindex="-1" role="dialog" aria-labelledby="collStuffLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contentLabel4">Subscribe When Available</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form form action="/item" method="post"  role="form" data-toggle="validator">
                <input type="hidden" id="subAction4" name="action" value="addMarioSub">
                <input type="hidden" id="SubIdItem4" name="idItem" value="4">
                <div class="modal-body">
                    <div class="container-fluid">
                        <div class="row">
                            <div class="col-sm-12">
                                <img class="img-fluid"src="./img/MARIO/5.jpg">
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="modalFormWrapper">

                                    <div class="form-group">
                                        <label for="formName4">What's your name?</label>
                                        <input type="text" class="form-control" id="formName4" name="name" placeholder="Name" required="required">
                                    </div>
                                    <div class="form-group">
                                        <label for="formEmail4">Where should we send your cool content?</label>
                                        <input type="email" class="form-control" id="formEmail4" name="email" placeholder="Email" required="required">
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-secondary">Subscribe</button>
                </div>
            </form>
    </div>
</div>
</div>

<!--Sold Out Modal5-->
<div class="modal fade" id="soldOutModal5" tabindex="-1" role="dialog" aria-labelledby="collStuffLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="contentLabel5">Subscribe When Available</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form form action="/item" method="post"  role="form" data-toggle="validator">
                <input type="hidden" id="subAction5" name="action" value="addMarioSub">
                <input type="hidden" id="SubIdItem5" name="idItem" value="5">
            <div class="modal-body">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-sm-12">
                            <img class="img-fluid"src="./img/MARIO/6.jpg">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="modalFormWrapper">

                                    <div class="form-group">
                                        <label for="formName5">What's your name?</label>
                                        <input type="text" class="form-control" id="formName5" name="name" placeholder="Name" required="required">
                                    </div>
                                    <div class="form-group">
                                        <label for="formEmail5">Where should we send your cool content?</label>
                                        <input type="email" class="form-control" id="formEmail5" name="email" placeholder="Email" required="required">
                                    </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-secondary">Subscribe</button>
            </div>
            </form>
        </div>
    </div>
</div>
</div>

</div>
</body>
</html>