<%--
  Created by IntelliJ IDEA.
  User: cjnora
  Date: 2018/10/3
  Time: 上午10:25
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!doctype html>
<html>
<head>
    <title>Oh!Oh!Shop|Order Detail</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.3/umd/popper.min.js" integrity="sha384-ZMP7rVo3mIykV+2+9J3UJ46jBk0WLaUAdn689aCwoqbBJiSnjAK/l8WvCWPIPm49" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/js/bootstrap.min.js" integrity="sha384-ChfqqxuZUCnJSK3+MXmPNIyE6ZbWh2IMqE241rYiqJxyMiZ6OW/JmZQ5stwEULTy" crossorigin="anonymous"></script>
    <style type="text/css">
        @import "main.css";
    </style>
</head>

<body>
<div class="container-fluid" onload="">
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
        <a class="navbar-brand" href="index.jsp">Oh!Oh!Shop</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarNavDropdown">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item">
                    <a class="nav-link" href="index.jsp">Home</a>
                </li>
                <li class="nav-item active">
                    <a class="nav-link" href="/item">MyCart</a><span class="sr-only">(current)</span>
                </li>
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Brands</a>
                    <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
                        <a class="dropdown-item" href="marioBrand.jsp">Mario</a>
                        <a class="dropdown-item" href="pokemonBrand.jsp">Pokemon</a>
                    </div>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="help.jsp">Help</a>
                </li>
            </ul>
            <ul class="navbar-nav ml-auto">
                <li class="nav-item">
                    <%--<form action="account" method="post" id="logoutForm" name="logoutForm" role="form" >--%>
                        <%--<input type="hidden" id="currentPage" name="currentPage">--%>
                        <%--<input type="hidden" id="action" name="action">--%>
                        <%--<%--%>
                            <%--if (session.getAttribute("user") != null) {--%>
                                <%--out.print("<a class=\"login-link\" href=\"#\" onclick=\"document.getElementById('action').value = 'logout';document.getElementById('currentPage').value = '/index.jsp';document.getElementById('logoutForm').submit();\">Logout</a>");--%>
                            <%--} else {--%>
                                <%--out.print("<a class=\"login-link\" href=\"login.jsp\">Login</a>");--%>
                            <%--}--%>
                        <%--%>--%>
                    <%--</form>--%>
                    <%--<form action="order" method="get" id="orderForm" name="orderForm" role="form" >--%>
                        <%--<input type="hidden" id="searchAction" name="searchAction" value="searchOrderByCurrentUser">--%>
                        <%--<%--%>
                            <%--if (session.getAttribute("user") != null) {--%>
                                <%--out.print("<a class=\"login-link\" href=\"#\" onclick=\"document.getElementById('orderForm').submit();\" >  Order</a>");--%>
                            <%--}--%>
                        <%--%>--%>
                    <%--</form>--%>
                </li>
            </ul>
        </div>
    </nav>

    <div class="container">
        <c:if test="${not empty message}">
            <div class="alert alert-success">
                    ${message}
            </div>
        </c:if>
        <form action="order" method="post" id="orderForm" role="form" name="orderForm" >
            <input type="hidden" id="idOrder" name="idOrder">
            <input type="hidden" id="action" name="action">
<c:choose>
    <c:when test="${not empty orderList}">
        <c:set var="orderNumber" value="${0}"/>
        <c:forEach var="order" items="${orderList}">
            <c:set var="orderNumber" value="${orderNumber + 1}"/>
            <c:set var="classSucess" value=""/>
            <c:if test ="${idOrder == order.id}">
                <c:set var="classSucess" value="info"/>
            </c:if>
        <table class="table table-striped">
            <thead>
            <tr>
                <th scope="col">Order Number</th>
                <th scope="col">Order Date</th>
                <th scope="col">Total</th>
                <th scope="col">Payment</th>
                <th scope="col">Delivery State</th>
                <th scope="col"></th>
                <th scope="col"></th>
            </tr>
            </thead>
            <tbody>
            <tr  class= "classSucess" data-toggle="collapse" data-toggle="collapse" data-target=".order${orderNumber}">

                <td>${order.id}</td>
                <td>${order.date}</td>
                <td>$${order.totalPrice}</td>
                <td>${order.payment.pay}</td>
                <td>non arrival</td>
                <td><Input type="submit" id="remove" value="REMOVE" class="btn btn-default btn-sm" onclick="document.getElementById('action').value = 'remove';document.getElementById('idOrder').value = '${order.id}';document.getElementById('orderForm').submit();"/></td>
                <td>+</td>
            </tr>
            </tbody>
        </table>
        <table class="table collapse order${orderNumber} col-centered" style="width:70%;text-align:center;">
            <thead class="thead-dark">
            <tr>
                <th scope="col">#</th>
                <th scope="col">Product</th>
                <th scope="col">Price</th>
                <th scope="col">Quantity</th>
                <th scope="col">Total</th>
            </tr>
            </thead>
            <c:forEach var="item" items="${order.itemList}">
            <tbody>
            <tr class= "classSucess" >
                <td>
                    <c:if test="${item.clas == 'Mario'}">
                        <c:forEach var="i" items="${marioList}">
                            <c:if test="${i.name == item.name}">
                                <img src="img/MARIO/${i.id}.jpg" style="width: 50px;height: 50px;"/>
                            </c:if>
                        </c:forEach>
                    </c:if>
                    <c:if test="${item.clas == 'Pokemon'}">
                        <c:forEach var="i" items="${pokemonList}">
                            <c:if test="${i.name == item.name}">
                                <img src="img/POKEMON/${i.id}.jpg" style="width: 50px;height: 50px;"/>
                            </c:if>
                        </c:forEach>
                    </c:if>
                    <c:if test="${item.clas == 'Charmander'}">
                        <c:forEach var="i" items="${charmanderList}">
                            <c:if test="${i.name == item.name}">
                                <img src="img/Charmander/${i.id}.jpg" style="width: 50px;height: 50px;"/>
                            </c:if>
                        </c:forEach>
                    </c:if>
                    <c:if test="${item.clas == 'Luigi'}">
                        <c:forEach var="i" items="${luigiList}">
                            <c:if test="${i.name == item.name}">
                                <img src="img/Luigi/${i.id}.jpg" style="width: 50px;height: 50px;"/>
                            </c:if>
                        </c:forEach>
                    </c:if>
                </td>
                <td>${item.name}</td>
                <td>$${item.price}</td>
                <td>${item.quantity}</td>
                <td>$${item.price * item.quantity}</td>
            </tr>
            </tbody>
        </c:forEach>
        </c:forEach>
        </table>
    </c:when>
    <c:otherwise>
        <br>
        <div class="alert alert-info">
            No order found matching your search criteria
        </div>
    </c:otherwise>
</c:choose>
        </form>
    </div>
</div>
</body>
</html>
