
<%--
  Created by IntelliJ IDEA.
  User: cjnora
  Date: 2018/9/27
  Time: 下午3:45
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="java.net.*" %>
<!DOCTYPE html>
<html lang="en">

<head>
  <title>Oh!Oh!Shop|Home</title>
  <!-- Required meta tags -->
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <!-- Bootstrap CSS -->
  <link rel="stylesheet" href="bootstrap-4/css/bootstrap-grid.min.css">
  <link rel="stylesheet" href="bootstrap-4/css/bootstrap.min.css">
  <!--CSS-->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
  <link rel="stylesheet" href="main.css">
</head>

<body>
<div class="container-fluid">
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
    <a class="navbar-brand" href="index.jsp">Oh!Oh!Shop</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavDropdown" aria-controls="navbarNavDropdown" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNavDropdown">
      <ul class="navbar-nav mr-auto">
        <li class="nav-item  active">
          <a class="nav-link" href="index.jsp">Home</a><span class="sr-only">(current)</span>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="/item">MyCart</a>
        </li>
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Brands</a>
          <div class="dropdown-menu" aria-labelledby="navbarDropdownMenuLink">
            <a class="dropdown-item" href="marioBrand.jsp">Mario</a>
            <a class="dropdown-item" href="pokemonBrand.jsp">Pokemon</a>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="help.jsp">Help</a>
        </li>
      </ul>
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <form action="account" method="post" id="logoutForm" name="logoutForm" role="form" >
              <input type="hidden" id="currentPage" name="currentPage">
              <input type="hidden" id="action" name="action">
                  <%
                          if (session.getAttribute("user") != null) {
                              out.print("<a class=\"login-link\" href=\"#\" onclick=\"document.getElementById('action').value = 'logout';document.getElementById('currentPage').value = '/index.jsp';document.getElementById('logoutForm').submit();\">Logout</a>");
                      } else {
                              out.print("<a class=\"login-link\" href=\"login.jsp\">Login</a>");
                          }
                    %>
            </form>
              <form action="order" method="get" id="orderForm" name="orderForm" role="form" >
                  <input type="hidden" id="searchAction" name="searchAction" value="searchOrderByCurrentUser">
                  <%
                      if (session.getAttribute("user") != null) {
                          out.print("<a class=\"login-link\" href=\"#\" onclick=\"document.getElementById('orderForm').submit();\" >  Order</a>");
                      }
                  %>
              </form>
          </li>
        </ul>
    </div>
  </nav>


  <!--Section One-->
  <div class="sectionLight">
    <div class="row">
      <div class="col-sm-12">
        <div class="jumbotron jumbotron-fluid">
          <div class="container">
            <h1 class="display-4">Welcome to Oh!Oh!Shop</h1>
            <p class="lead">Since 2018 Sep. to 2018 Jan.(or longer....) </p>
          </div>
        </div>
      </div>
    </div>

    <div class="row">
      <div class="col"></div>
      <div class="col-sm-12 col-md-6">
        <div id="mainCarousel" class="carousel slide" data-ride="carousel">
          <div class="carousel-inner">
            <div class="carousel-item active">
              <a href="pokemonBrand.jsp"><img class="d-block w-100" src="img/POKEMON/6.jpg" alt="First slide" ></a>
            </div>
            <div class="carousel-item">
              <a href="marioBrand.jsp"><img class="d-block w-100" src="img/MARIO/5.jpg" alt="Second slide" href="marioBrand.jsp"></a>
              <div class="carousel-caption d-none d-md-block">
                <h5 style="color: yellow;">Shop for life!</h5>
                <p style="color: yellow;">Show me some products</p>
              </div>
            </div>
            <div class="carousel-item">
              <a href="pokemonBrand.jsp"><img class="d-block w-100" src="img/POKEMON/2.jpg" alt="Third slide" href="pokemonBrand.jsp"></a>
            </div>
          </div>
          <a class="carousel-control-prev" href="#mainCarousel" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
          </a>
          <a class="carousel-control-next" href="#mainCarousel" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
          </a>
        </div>
      </div>
      <div class="col"></div>
    </div>
  </div>
  <!-- Section Two-->
  <div class="sectionDark">
    <div class="row">
      <div class="col-sm-12 col-md-1 offset-md-2">
        <img class="img-fluid" src="img/POKEMON/2.jpg">
      </div>
      <div class="col-sm-12 col-md-6">
        <div class="row">
          <div class="col-sm-12">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
        </div>
        <div class="row">
          <div class="col-sm-12 col-md-9">
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
          </div>
          <div class="col-sm-12 col-md-3">
            <img class="img-fluid" src="img/MARIO/3.jpg">
          </div>
        </div>
      </div>
      <div class="col"></div>
    </div>
    <div class="row">
      <div class="col-sm-12">
        <div class="coolStuffWrapper">
          <button type="button" class="btn btn-lg coolStuffBtn" data-toggle="modal" data-target="#coolStuffModal">Get Cool Stuff!</button>
        </div>
      </div>
    </div>
  </div>

  <div class="sectionLight">
    <div class="row">
      <div class="col-sm-12">
        <div class="row">
          <div class="col-sm-12 col-lg-4">
            <img class="img-fluid imgCenter" src="img/2.jpg">
            <div class="row">
              <div class="col-sm-12">
                <button type="button" class="btn btn-danger btn-lg btnCenter" data-toggle="modal" data-target=".kf-modal-lg">Template</button>
              </div>
            </div>
          </div>
            <div class="col-sm-12 col-lg-4">
                <img class="img-fluid imgCenter" src="img/3.jpg">
                <div class="row">
                    <div class="col-sm-12">
                        <button type="button" class="btn btn-danger btn-lg btnCenter" data-toggle="modal" data-target=".b-modal-md">Abstract Factory</button>
                    </div>
                </div>
            </div>
          <div class="col-sm-12 col-lg-4">
            <img class="img-fluid imgCenter" src="img/4.jpg">
            <div class="row">
              <div class="col-sm-12">
                <button type="button" class="btn btn-danger btn-lg btnCenter" data-toggle="modal" data-target=".bscrook-modal-sm">Abstract Factory</button>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  <!--Footer-->
  <footer class="footer bg-dark">
    <div class="container-fluid">
      <div class="row">
        <div class="col-sm-12 col-lg-3">
          <%--<img class="img-fluid footerImg" src="img/POKEMON/1.jpg">--%>
              <nav class="navbar navbar-expand-lg navbar-dark bg-dark">
              <a class="navbar-brand" href="#">Oh!Oh!Shop</a>
                  </nav>
        </div>
        <div class="col-sm-12 col-lg-9 socialWrapper">
          <a href="https://facebook.com" target="_blank">
            <i class="fa fa-facebook-square fa-3x" href="https://www.facebook.com/小每攵-1022420044468384/"></i>
          </a>
          <i class="fa fa-google-plus-square fa-3x"></i>
          <i class="fa fa-instagram fa-3x"></i>
        </div>
      </div>
    </div>
  </footer>

  <!-- Modal -->

  <!--Cool Stuff Modal-->
  <div class="modal fade" id="coolStuffModal" tabindex="-1" role="dialog" aria-labelledby="collStuffLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="contentLabel">Get Exclusive Content</h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <div class="container-fluid">
            <div class="row">
              <div class="col-sm-12">
                <img class="img-fluid"src="img/POKEMON/1.jpg">
              </div>
            </div>
            <div class="row">
              <div class="col-sm-12">
                <div class="modalFormWrapper">
                  <form>
                    <div class="form-group">
                      <label for="formName">What's your name?</label>
                      <input type="text" class="form-control" id="formName" placeholder="Name">
                    </div>
                    <div class="form-group">
                      <label for="formEmail">Where should we send your cool content?</label>
                      <input type="text" class="form-control" id="formEmail" placeholder="Email">
                    </div>
                  </form>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        </div>
      </div>
    </div>
  </div>
</div>

<!--Kickflip Modal-->
<div class="modal fade kf-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <div class="embed-responsive embed-responsive-16by9">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/RxF3wKPuXEY" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <p>Template pattern is easy. Hook method, abstract method, concrete method, Oh!Oh! #BOOM</p>
    </div>
  </div>
</div>

<!--360 Flip Modal-->
<div class="modal fade b-modal-md" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-md">
    <div class="modal-content">
      <div class="embed-responsive embed-responsive-16by9">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/ub0DXaeV6hA" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <p>Factory pattern is easy. Just a factory. Oh!Oh! #BOOM</p>
    </div>
  </div>
</div>

<!-- BS Crook Modal-->
<div class="modal fade bscrook-modal-sm" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
  <div class="modal-dialog modal-sm">
    <div class="modal-content">
      <div class="embed-responsive embed-responsive-16by9">
          <iframe width="560" height="315" src="https://www.youtube.com/embed/xbjAsdAK4xQ" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
      </div>
      <p>Abstract factory pattern is easy. Family or related, productA, productB, factory1, factory2, can we create productA3 without factory3? Oh!Oh! #BOOM</p>
    </div>
  </div>
</div>






<!-- jQuery first, then Tether, then Bootstrap JS. -->
<script src="https://code.jquery.com/jquery-3.1.1.slim.min.js" integrity="sha384-A7FZj7v+d/sdmMqp/nOQwliLvUsJfDHW+k9Omg/a/EheAdgtzNs3hpfag6Ed950n" crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/tether/1.4.0/js/tether.min.js" integrity="sha384-DztdAPBWPRXSA/3eYEEUWrWCy7G5KFbe8fFjk5JAIxUYHKkDx6Qin1DkWx51bBrb" crossorigin="anonymous"></script>
<script src="bootstrap-4/js/bootstrap.min.js"></script>
</body>

</html>
