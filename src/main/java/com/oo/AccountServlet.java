package com.oo;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(
        name = "AccountServlet",
        urlPatterns = {"/account"}
)
public class AccountServlet extends HttpServlet {

    AccountService accountService = AccountService.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("searchAction");
        if (action!=null){
            switch (action) {
                case "searchById":
                    searchAccountById(req, resp);
                    break;
                case "searchByEmail":
                    searchAccountByEmail(req, resp);
                    break;
                case "searchCurrentUser":
                    searchCurrentUser(req,resp);
                    break;
            }
        }else{
            List<Account> result = accountService.getAllAccount();
            forwardListItem(req, resp, result);
        }
    }
    private void searchCurrentUser(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String currentPage = req.getParameter("currentPage");
        long currentUser = accountService.getCrrentUser();

        String nextJSP = currentPage;
        req.setAttribute("currentUser", currentUser);
//        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
//        dispatcher.forward(req, resp);
    }
    private void searchAccountById(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        long uid = Integer.valueOf(req.getParameter("uid"));
        Account account = null;
        try {
            account = accountService.getAccount(uid);
        } catch (Exception ex) {
            Logger.getLogger(ItemServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        req.setAttribute("account", account);
        req.setAttribute("action", "edit");
        String nextJSP = "/mario.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        dispatcher.forward(req, resp);
    }

    private void searchAccountByEmail(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String email = req.getParameter("email");
        List<Account> result = accountService.searchAccountByEmail(email);
        forwardListItem(req, resp, result);
    }

    private void forwardListItem(HttpServletRequest req, HttpServletResponse resp, List accountList)
            throws ServletException, IOException {
        String nextJSP = "/login.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        req.setAttribute("accountList", accountList);
        System.out.println(accountService.getCrrentUser());
        req.setAttribute("currentUser", accountService.getCrrentUser());
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String action = req.getParameter("action");
        switch (action) {
            case "create":
                createAccountAction(req, resp);
                break;
            case "edit":
                editAccountAction(req, resp);
                break;
            case "remove":
                removeAccountByUid(req, resp);
                break;
            case"login":
                login(req,resp);
                break;
            case "logout":
                logout(req,resp);
                break;
            case "checkCurrentUser":
                checkCurrentUser(req, resp);
                break;
        }
        req.setAttribute("currentUser", accountService.getCrrentUser());


    }

    private void login(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");

        Account result = accountService.findAccountByEmailAndPassword(email,password);
        if (accountService.login(result)) {
            req.setAttribute("currentUid", result.getUid());
            String message = "Login successfully!";
            req.setAttribute("message", message);
            String nextJSP = "/index.jsp";
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
            req.setAttribute("account", result);
            System.out.println(accountService.getCrrentUser());
            req.setAttribute("currentUser", accountService.getCrrentUser());
            resp.setContentType("text/html");
            HttpSession session = req.getSession(true);
            session.setAttribute("user", accountService.getCrrentUser());
            session.setMaxInactiveInterval(60 * 60 * 24 * 365); // 30 seconds
            resp.sendRedirect("index.jsp");
//            dispatcher.forward(req, resp);
        } else {
            String message = "Login fail!";
            req.setAttribute("message", message);
            String nextJSP = "/login.jsp";
            System.out.println(accountService.getCrrentUser());
            req.setAttribute("currentUser", accountService.getCrrentUser());
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
            dispatcher.forward(req, resp);
        }


    }

    private void logout(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
            String currentPage = req.getParameter("currentPage");
            accountService.logout();
            String message = "Logout!";
            req.setAttribute("message", message);
        System.out.println(accountService.getCrrentUser());
        req.setAttribute("currentUser", accountService.getCrrentUser());
        System.out.println("thanq you!!, Your session was destroyed successfully!!");
        resp.setContentType("text/html");
        HttpSession session = req.getSession(false);
        session.removeAttribute("user");
        session.getMaxInactiveInterval();
        String nextJSP = currentPage;
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        dispatcher.forward(req, resp);

    }
    private void createAccountAction(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        Account account;

        List<Account> accountList = accountService.getAllAccount();

        long result = accountService.findAccountInListByEmail(email);


        if (result == -1){
            account = new Account(email, password);
            long uid = accountService.createAccount(account);
            req.setAttribute("uid", uid);
            String message = "The new account has been successfully created.";
            req.setAttribute("message", message);
            forwardListItem(req, resp, accountList);
        }  else {
            String message = null;
                message = "The account has already exist.";
            req.setAttribute("uid", result);
            req.setAttribute("message", message);
            String nextJSP = "/register.jsp";
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
            dispatcher.forward(req, resp);
        }

    }

    private void editAccountAction(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String email = req.getParameter("email");
        String password = req.getParameter("password");
        long uid = Integer.valueOf(req.getParameter("uid"));
        Account account = new Account(email, password);
        account.setUid(uid);
        boolean success = accountService.updateAccount(account);
        String message = null;
        if (success) {
            message = "The account has been successfully updated.";
        }
        List<Account> accountList = accountService.getAllAccount();
        req.setAttribute("uid", uid);
        req.setAttribute("message", message);
        forwardListItem(req, resp, accountList);
    }

    private void removeAccountByUid(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        long uid = Integer.valueOf(req.getParameter("uid"));
        boolean confirm = accountService.deleteAccount(uid);
        if (confirm) {
            String message = "The account has been successfully removed.";
            req.setAttribute("message", message);
        }
        List<Account> accountList = accountService.getAllAccount();
        forwardListItem(req, resp, accountList);
    }
    private void checkCurrentUser(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        long currentUser = accountService.getCrrentUser();
        if (currentUser == -1){
            String nextJSP = "/login.jsp";
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
            System.out.println(accountService.getCrrentUser());
            dispatcher.forward(req, resp);
        }else {
            String nextJSP = "/payment.jsp";
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
            System.out.println(accountService.getCrrentUser());
            dispatcher.forward(req, resp);
        }
    }
}
