package com.oo;

import java.util.ArrayList;
import java.util.List;

public class ItemList {
    private static List<Item> itemList = new ArrayList();
    private static List<Item> marioList = new ArrayList<>();
    private static List<Item> pokemonList = new ArrayList<>();
    private static List<Item> charmanderList = new ArrayList<>();
    private static List<Item> luigiList = new ArrayList<>();

    private ItemList() {


    }

    static {
        //mario
        marioList.add(new Item("Mario","Mario Toy", 5,10,1));
        marioList.add(new Item("Mario","Mario Toy 3in1", 3,450,2));
        marioList.add(new Item("Mario","Sitting Mario", 0,150,3));
        marioList.add(new Item("Mario","Toolman Mario", 0,200,4));
        marioList.add(new Item("Mario","Mario Doll", 0,50,5));
        //pokemon
        pokemonList.add(new Item("Pokemon","Attack Squirtle", 5,100,1));
        pokemonList.add(new Item("Pokemon","Squirtle Box", 3,100,2));
        pokemonList.add(new Item("Pokemon","Pokemon Can", 0,150,3));
        pokemonList.add(new Item("Pokemon","Squirtle Ｍagnet", 0,10,4));
        pokemonList.add(new Item("Pokemon","Squirtle Bang", 0,1000,5));
        //charmander
        charmanderList.add(new Item("Charmander","Happy Charmander", 2,100,1));
        charmanderList.add(new Item("Charmander","CharmanderCharm", 0,20,2));
        charmanderList.add(new Item("Charmander","Charmander Doll", 0,120,3));
        charmanderList.add(new Item("Charmander","Charmander Magnet", 0,10,4));
        charmanderList.add(new Item("Charmander","Charmander Box", 0,100,5));
        charmanderList.add(new Item("Charmander","Charmander Can", 0,150,6));
        //luigi
        luigiList.add(new Item("Luigi","Luigi Hat", 3,50,1));
        luigiList.add(new Item("Luigi","Luigi Great", 0,100,2));
        luigiList.add(new Item("Luigi","Luigi Jumpping", 0,100,3));
        luigiList.add(new Item("Luigi","Luigi Cart", 0,60,4));
        luigiList.add(new Item("Luigi","Scared Luigi", 0,120,5));
        luigiList.add(new Item("Luigi","Pipe Luigi", 0,100,6));

    }

    public static List <Item> getItemInstance() {
        return itemList;
    }

    public static List <Item> getMarioInstance() {
        return marioList;
    }

    public static List <Item> getPokemonInstance() {
        return pokemonList;
    }

    public static List <Item> getCharmanderInstance() {
        return charmanderList;
    }

    public static List <Item> getLuigiInstance() {
        return luigiList;
    }


    public static  void clearItemList() {
        itemList.clear();
    }



}
