package com.oo;

import sun.text.resources.sv.JavaTimeSupplementary_sv;

import java.time.Instant;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;

public  class ItemService {

    List<Item> itemList = ItemList.getItemInstance();
    List<Item> marioList = ItemList.getMarioInstance();
    List<Item> pokemonList = ItemList.getPokemonInstance();
    List<Item> charmanderList = ItemList.getCharmanderInstance();
    List<Item> luigiList = ItemList.getLuigiInstance();
    List<Subscriber> marioSubList = SubscriberList.getMarioSubInstance();
    List<Subscriber> pokemonSubList = SubscriberList.getPokemonInstance();
    List<Subscriber> charmanderSubList = SubscriberList.getCharmanderSubInstance();
    List<Subscriber> luigiSubList = SubscriberList.getLuigiInstance();
    List<Item> undoList;

    //Singleton
    private volatile static ItemService instance = null;
    public synchronized static ItemService getInstance() {
        if (instance == null) {
            instance = new ItemService();
        }
        return instance;
    }
    //Get all mario.
    public List<Item> getAllItem() {
        return itemList;
    }

    public List<Item> getAllMario() {
        return marioList;
    }

    public List<Subscriber> getMarioSubList() {
        return marioSubList;
    }


    public boolean addMarioSub(Subscriber sub) {
        boolean b = true;

        for (Subscriber s : marioSubList) {
            if ((s.getEmail().equals(sub.getEmail())) && (s.getItemId() == sub.getItemId())) {
                b = false;
            }
        }
        if (b) {
            marioSubList.add(sub);
        }
        return b;
    }

    public boolean removeMarioSub(long idItem) {

        Predicate<Subscriber> sub = e -> e.getItemId() == idItem;
        if (marioSubList.removeIf(sub)) {
            return true;
        } else {
            return false;
        }

    }

    public List<Item> getAllPokemon() {
        return pokemonList;
    }

    public List<Subscriber> getPokemonSubList() {
        return pokemonSubList;
    }

    public boolean addPokemonSub(Subscriber sub) {
        boolean b = true;

        for (Subscriber s : pokemonSubList) {
            if ((s.getEmail().equals(sub.getEmail())) && (s.getItemId() == sub.getItemId())) {
                b = false;
            }
        }
        if (b) {
            pokemonSubList.add(sub);
        }
        return b;
    }

    public boolean removePokemonSub(long idItem) {
        Predicate<Subscriber> sub = e -> e.getItemId() == idItem;
        if (pokemonSubList.removeIf(sub)) {
            return true;
        } else {
            return false;
        }
    }

    public List<Item> getAllCharmander() {
        return charmanderList;
    }

    public List<Subscriber> getCharmanderSubList() {
        return charmanderSubList;
    }


    public boolean addCharmanderSub(Subscriber sub) {

        boolean b = true;

        for (Subscriber s : charmanderSubList) {
            if ((s.getEmail().equals(sub.getEmail())) && (s.getItemId() == sub.getItemId())) {
                b = false;
            }
        }
        if (b) {
            charmanderSubList.add(sub);
        }
        return b;
    }

    public boolean removeCharmanderSub(long idItem) {

        Predicate<Subscriber> sub = e -> e.getItemId() == idItem;
        if (charmanderSubList.removeIf(sub)) {
            return true;
        } else {
            return false;
        }

    }

    public List<Item> getAllLuigi() {
        return luigiList;
    }

    public List<Subscriber> getLuigiSubList() {
        return luigiSubList;
    }


    public boolean addLuigiSub(Subscriber sub) {

        boolean b = true;

        for (Subscriber s : luigiSubList) {
            if ((s.getEmail().equals(sub.getEmail())) && (s.getItemId() == sub.getItemId())) {
                b = false;
            }
        }
        if (b) {
            luigiSubList.add(sub);
        }
        return b;
    }

    public boolean removeLuigiSub(long idItem) {

        Predicate<Subscriber> sub = e -> e.getItemId() == idItem;
        if (luigiSubList.removeIf(sub)) {
            return true;
        } else {
            return false;
        }

    }

    //Get all item whose Name or Last Name match the searchParam, order by name and price.
    public List<Item> searchItemByName(String searchParam) {
//        Comparator<Item> groupByComparator = Comparator.comparing(Item::getName);
//        List<Item> result = itemList
//                .stream()
//                .filter(e -> e.getName().equalsIgnoreCase(searchParam))
//                .sorted(groupByComparator)
//                .collect(Collectors.toList());
        List<Item> result = new ArrayList<>();
        for(Item i:itemList) {
            if(i.getName().toLowerCase().contains(searchParam.toLowerCase())) {
                result.add(i);
            }
        }
        return result;
    }

    public List<Item> searchItemByNameAndClas(String searchName, String searchClas) {
        Comparator<Item> groupByComparator = Comparator.comparing(Item::getName).thenComparing(Item::getClas);
        List<Item> result = itemList
                .stream()
                .filter(e -> e.getName().equalsIgnoreCase(searchName) && e.getClas().equalsIgnoreCase(searchClas))
                .sorted(groupByComparator)
                .collect(Collectors.toList());
        return result;
    }

    public long findItemInListByClasAndName(String searchName, String searchClas) {
        Item target = null;
        for (Item item : itemList) {
            if ((item.getClas().equals(searchClas)) && (item.getName().equals(searchName))) {
                target = item;
            }
        }
        if (target == null) {
            return -1;
        } else {
            return target.getId();
        }
    }

    public int findItemById(long id) {
        Item target = null;
        for (Item item : itemList) {
            if (item.getId() == id) {
                target = item;
            }
        }
        if (target == null) {
            return 1;
        } else {
            return target.getQuantity();
        }
    }

    //Get the item by id
    public Item getItem(long id) throws Exception {
        Optional<Item> match
                = itemList.stream()
                .filter(e -> e.getId() == id)
                .findFirst();
        if (match.isPresent()) {
            return match.get();
        } else {
            throw new Exception("The item id " + id + " not found");
        }
    }

    public double getTotalPrice() {
        double price = 0;
        if (!(itemList.isEmpty())) {
            for (Item item : itemList) {
                price += (item.getPrice() * item.getQuantity());
            }
        }
        return price;
    }

    public long addItem(Item item) {
        undoList = new ArrayList<>();
        for (Item i : itemList) {
            undoList.add(i);
        }
        itemList.add(item);
        return item.getId();
    }

    public boolean updateItem(Item item) {
        undoList = new ArrayList<>();
        for (Item i : itemList) {
            undoList.add(i);
        }
        int matchIdx = 0;
        Optional<Item> match = itemList.stream()
                .filter(c -> c.getId() == item.getId())
                .findFirst();
        if (match.isPresent()) {
            matchIdx = itemList.indexOf(match.get());
            itemList.set(matchIdx, item);
            System.out.println(undoList);
            return true;
        } else {
            return false;
        }
    }


    public boolean updateMario(Item item) {
//        undoList = new ArrayList<>();
//        for (Item i : itemList) {
//            undoList.add(i);
//        }
        int matchIdx = 0;
        Optional<Item> match = marioList.stream()
                .filter(c -> c.getId() == item.getId())
                .findFirst();
        if (match.isPresent()) {
            matchIdx = marioList.indexOf(match.get());
            marioList.set(matchIdx, item);
//            System.out.println(undoList);
            return true;
        } else {
            return false;
        }
    }

    public boolean updatePokemon(Item item) {
//        undoList = new ArrayList<>();
//        for (Item i : itemList) {
//            undoList.add(i);
//        }
        int matchIdx = 0;
        Optional<Item> match = pokemonList.stream()
                .filter(c -> c.getId() == item.getId())
                .findFirst();
        if (match.isPresent()) {
            matchIdx = pokemonList.indexOf(match.get());
            pokemonList.set(matchIdx, item);
//            System.out.println(undoList);
            return true;
        } else {
            return false;
        }
    }

    public boolean updateCharmander(Item item) {
//        undoList = new ArrayList<>();
//        for (Item i : itemList) {
//            undoList.add(i);
//        }
        int matchIdx = 0;
        Optional<Item> match = charmanderList.stream()
                .filter(c -> c.getId() == item.getId())
                .findFirst();
        if (match.isPresent()) {
            matchIdx = charmanderList.indexOf(match.get());
            charmanderList.set(matchIdx, item);
//            System.out.println(undoList);
            return true;
        } else {
            return false;
        }
    }

    public boolean updateLuigi(Item item) {
//        undoList = new ArrayList<>();
//        for (Item i : itemList) {
//            undoList.add(i);
//        }
        int matchIdx = 0;
        Optional<Item> match = luigiList.stream()
                .filter(c -> c.getId() == item.getId())
                .findFirst();
        if (match.isPresent()) {
            matchIdx = luigiList.indexOf(match.get());
            luigiList.set(matchIdx, item);
//            System.out.println(undoList);
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteItem(long id) {
        undoList = new ArrayList<>();
        for (Item i : itemList) {
            undoList.add(i);
        }
        Predicate<Item> item = e -> e.getId() == id;
        if (itemList.removeIf(item)) {
            return true;
        } else {
            return false;
        }
    }

    public void undo() {
        ItemList.clearItemList();
        for (Item i : undoList) {
            itemList.add(i);
        }
        System.out.println(itemList);
    }

    public void clearItemList() {
        ItemList.clearItemList();
    }

    public void clearUndoList() {
        undoList.clear();
    }

    public void sendMail(String email,String message,String subject) {
        final String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
        // Get a Properties object
        Properties props = System.getProperties();
        props.setProperty("mail.smtp.host", "smtp.gmail.com");
        props.setProperty("mail.smtp.socketFactory.class", SSL_FACTORY);
        props.setProperty("mail.smtp.socketFactory.fallback", "false");
        props.setProperty("mail.smtp.port", "465");
        props.setProperty("mail.smtp.socketFactory.port", "465");
        props.put("mail.smtp.auth", "true");
        props.put("mail.debug", "true");
        props.put("mail.store.protocol", "pop3");
        props.put("mail.transport.protocol", "smtp");
        final String username = "ohohshoplolz@gmail.com";//
        final String password = "ohohshop";
        try {
            Session session = Session.getDefaultInstance(props,
                    new Authenticator() {
                        protected PasswordAuthentication getPasswordAuthentication() {
                            return new PasswordAuthentication(username, password);
                        }
                    });

            // -- Create a new message --
            Message msg = new MimeMessage(session);

            // -- Set the FROM and TO fields --
            msg.setFrom(new InternetAddress("ohohshop@ohohshop.com"));
            msg.setRecipients(Message.RecipientType.TO,
                    InternetAddress.parse(email, false));
            msg.setSubject(subject);
            msg.setText(message);
            msg.setSentDate(new Date());
            Transport.send(msg);
            System.out.println("Message sent.");
        } catch (MessagingException e) {
            System.out.println("Erreur d'envoi, cause: " + e);
        }
    }

}
