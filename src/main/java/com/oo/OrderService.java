package com.oo;

import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class OrderService {


    List<Order> orderList = OrderList.getOrderInstance();

    List<Item> itemList = ItemList.getItemInstance();

    //Singleton
    private volatile static OrderService instance = null;

    public synchronized  static OrderService getInstance() {
        if (instance == null ) {
            instance = new OrderService();
        }
        return instance;
    }

    //Get all mario.
    public List<Order> getAllOrder() {
        return orderList;
    }




    //Get all item whose Name or Last Name match the searchParam, order by name and price.
    public List<Order> searchOrderByAccount(String searchParam) {
        Comparator<Order> groupByComparator = Comparator.comparing(Order::getAccount);
        List<Order> result = orderList
                .stream()
                .filter(e -> e.getAccount().equalsIgnoreCase(searchParam))
                .sorted(groupByComparator)
                .collect(Collectors.toList());
        return result;
    }

    public List<Order> searchOrderByDate(String searchParam) {
//        Comparator<Order> groupByComparator = Comparator.comparing(Order::getDate);
////        List<Order> result = orderList
////                .stream()
////                .filter(e -> e.getAccount().equalsIgnoreCase(searchParam))
////                .sorted(groupByComparator)
////                .collect(Collectors.toList());
        List<Order> result = new ArrayList<>();
        for(Order i:orderList) {
            if((i.getDate()).equals(searchParam)) {
                result.add(i);
            }
        }
        return result;
    }

    public long findInListByAccount(String searchParam) {
        Order target = null;
        for(Order order:orderList) {
            if ((order.getAccount().equals(searchParam))) {
                target = order;
            }
        }
        if (target == null) {
            return -1;
        } else {
            return target.getId();
        }
    }

    public List<Item> findItemListById(long id) {
        Order target = null;
        for (Order order : orderList) {
            if (order.getId() == id) {
                target = order;
            }
        }
        if (target == null) {
            return null;
        } else {
            return target.getItemList();
        }
    }


    //Get the item by id
    public Order getOrder(long id) throws Exception {
        Optional<Order> match
                = orderList.stream()
                .filter(e -> e.getId() == id)
                .findFirst();
        if (match.isPresent()) {
            return match.get();
        } else {
            throw new Exception("The order id " + id + " not found");
        }
    }

    public long addOrder(Order order) {
        orderList.add(order);
        return order.getId();
    }

    public boolean updateOrder(Order order) {
        int matchIdx = 0;
        Optional<Order> match = orderList.stream()
                .filter(c -> c.getId() == order.getId())
                .findFirst();
        if (match.isPresent()) {
            matchIdx = orderList.indexOf(match.get());
            orderList.set(matchIdx, order);
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteOrder(long id) {
        Predicate<Order> order = e -> e.getId() == id;
        if (orderList.removeIf(order)) {
            return true;
        } else {
            return false;
        }
    }
}
