package com.oo;

import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

public class Order {

    private long id;
    private String date;
    private String account;
    private List<Item> itemList;
    private double totalPrice;
    private Payment payment;
    private static final AtomicLong counter = new AtomicLong(1234);

    public Order(String date,String account, List<Item> itemList, double totalPrice, Payment payment, long id) {
        this.date = date;
        this.account = account;
        this.itemList = itemList;
        this.totalPrice = totalPrice;
        this.payment = payment;
        this.id = id;
    }

    public Order(String date,String account, List<Item> itemList, double totalPrice, Payment payment) {
        this.date = date;
        this.account = account;
        this.itemList = itemList;
        this.totalPrice = totalPrice;
        this.payment = payment;
        this.id  = counter.incrementAndGet();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAccount() {
        return account;
    }

    public List<Item> getItemList() {
        return itemList;
    }

    public double getTotalPrice() {
        return totalPrice;
    }

    public Payment getPayment() {
        return payment;
    }

    public String getDate() {
        return date;
    }


    @Override
    public String toString() {
        return "Order{" +"date="+date+ ", id=" + id + ", account=" + account +
                ", itemList=" + itemList + ", totalPrice=" + totalPrice + ", payment=" + payment + '}';
    }

}