package com.oo;

import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class AccountService {
    private List<Account> accountList = AccountList.getAccountList();

    //Singleton
    private volatile static  AccountService instance = null;
    private AccountService(){}
    public synchronized static AccountService getInstance() {
        if(instance == null) {
            instance = new AccountService();
        }
        return instance;
    }
    //Get all account.
    public List<Account> getAllAccount() {
        return accountList;
    }

    //Get all item whose Name or Last Name match the searchParam, order by name and price.
    public List<Account> searchAccountByEmail(String searchParam) {
        Comparator<Account> groupByComparator = Comparator.comparing(Account::getAccount);
        List<Account> result = accountList
                .stream()
                .filter(e -> e.getAccount().equalsIgnoreCase(searchParam))
                .sorted(groupByComparator)
                .collect(Collectors.toList());
        return result;
    }

    public Account findAccountByEmailAndPassword(String searchMail, String searchPassword) {
        Account target = null;
        for(Account account:accountList) {
            if (account.getAccount().equals(searchMail) && account.getPassword().equals(searchPassword)) {
                target = account;
            }
        }
       return target;
    }

    public long findAccountInListByEmail(String email) {
        Account target = null;
        for(Account account:accountList) {
            if (account.getAccount().equals(email)) {
                target = account;
            }
        }
        if (target == null) {
            return -1;
        } else {
            return target.getUid();
        }
    }


    //Get the item by id
    public Account getAccount(long id) throws Exception {
        Optional<Account> match
                = accountList.stream()
                .filter(e -> e.getUid() == id)
                .findFirst();
        if (match.isPresent()) {
            return match.get();
        } else {
            throw new Exception("The user id " + id + " not found");
        }
    }

    public long getCrrentUser() {

        return AccountList.getCurrentUsr();

    }

    public void logout() {
        AccountList.setCurrentUsr(-1);
    }

    public boolean login(Account account) {
        try {
            AccountList.setCurrentUsr(account.getUid());
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public long createAccount(Account account) {

        accountList.add(account);
        return account.getUid();
    }
    public boolean updateAccount(Account account) {
        int matchIdx = 0;
        Optional<Account> match = accountList.stream()
                .filter(c -> c.getUid() == account.getUid())
                .findFirst();
        if (match.isPresent()) {
            matchIdx = accountList.indexOf(match.get());
            accountList.set(matchIdx, account);
            return true;
        } else {
            return false;
        }
    }

    public boolean deleteAccount(long uid) {
        Predicate<Account> account = e -> e.getUid() == uid;
        if (accountList.removeIf(account)) {
            return true;
        } else {
            return false;
        }
    }
}
