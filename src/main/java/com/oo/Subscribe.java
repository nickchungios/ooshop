//Observer Pattern

package com.oo;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

interface Subscribe { // Observerable
    public boolean registerSubscriber(Subscriber sub);
    public boolean removeSubscriber(long idItem);
    public void notifySubscriber();
}

class MarioSub implements Subscribe {

    private List<Subscriber> subscribers;
    private String subject;
    private String content;
    private long idItem;

    public MarioSub() {
        this.subscribers = SubscriberList.getMarioSubInstance();
    }

    public void sendMail(long idItem,String content,String subject)
    {
        this.content = content;
        this.idItem  = idItem;
        notifySubscriber();
    }

    @Override
    public boolean registerSubscriber(Subscriber sub) {
        boolean b = true;

        for (Subscriber s : subscribers) {
            if ((s.getEmail().equals(sub.getEmail())) && (s.getItemId() == sub.getItemId())) {
                b = false;
            }
        }
        if (b) {
            subscribers.add(sub);
        }
        return b;
    }

    @Override
    public boolean removeSubscriber(long idItem) {
        Predicate<Subscriber> sub = e -> e.getItemId() == idItem;
        if (subscribers.removeIf(sub)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void notifySubscriber() {
        for(Subscriber s: subscribers)
            if(s.getItemId() == idItem) {
               s.update(s.getEmail(),content,subject);
            }
    }
}

class PokemonSub implements Subscribe {

    private List<Subscriber> subscribers;
    private String subject;
    private String content;
    private long idItem;

    public PokemonSub() {
        this.subscribers = SubscriberList.getMarioSubInstance();
    }

    public void sendMail(long idItem,String content,String subject)
    {
        this.content = content;
        this.idItem  = idItem;
        notifySubscriber();
    }

    @Override
    public boolean registerSubscriber(Subscriber sub) {
        boolean b = true;

        for (Subscriber s : subscribers) {
            if ((s.getEmail().equals(sub.getEmail())) && (s.getItemId() == sub.getItemId())) {
                b = false;
            }
        }
        if (b) {
            subscribers.add(sub);
        }
        return b;
    }

    @Override
    public boolean removeSubscriber(long idItem) {
        Predicate<Subscriber> sub = e -> e.getItemId() == idItem;
        if (subscribers.removeIf(sub)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void notifySubscriber() {
        for(Subscriber s: subscribers)
            if(s.getItemId() == idItem) {
                s.update(s.getEmail(),content,subject);
            }
    }
}

class LuigiSub implements Subscribe {

    private List<Subscriber> subscribers;
    private String subject;
    private String content;
    private long idItem;

    public LuigiSub() {
        this.subscribers = SubscriberList.getMarioSubInstance();
    }

    public void sendMail(long idItem,String content,String subject)
    {
        this.content = content;
        this.idItem  = idItem;
        notifySubscriber();
    }

    @Override
    public boolean registerSubscriber(Subscriber sub) {
        boolean b = true;

        for (Subscriber s : subscribers) {
            if ((s.getEmail().equals(sub.getEmail())) && (s.getItemId() == sub.getItemId())) {
                b = false;
            }
        }
        if (b) {
            subscribers.add(sub);
        }
        return b;
    }

    @Override
    public boolean removeSubscriber(long idItem) {
        Predicate<Subscriber> sub = e -> e.getItemId() == idItem;
        if (subscribers.removeIf(sub)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void notifySubscriber() {
        for(Subscriber s: subscribers)
            if(s.getItemId() == idItem) {
                s.update(s.getEmail(),content,subject);
            }
    }
}

class CharmanderSub implements Subscribe {

    private List<Subscriber> subscribers;
    private String subject;
    private String content;
    private long idItem;

    public CharmanderSub() {
        this.subscribers = SubscriberList.getMarioSubInstance();
    }

    public void sendMail(long idItem,String content,String subject)
    {
        this.content = content;
        this.idItem  = idItem;
        notifySubscriber();
    }

    @Override
    public boolean registerSubscriber(Subscriber sub) {
        boolean b = true;

        for (Subscriber s : subscribers) {
            if ((s.getEmail().equals(sub.getEmail())) && (s.getItemId() == sub.getItemId())) {
                b = false;
            }
        }
        if (b) {
            subscribers.add(sub);
        }
        return b;
    }

    @Override
    public boolean removeSubscriber(long idItem) {
        Predicate<Subscriber> sub = e -> e.getItemId() == idItem;
        if (subscribers.removeIf(sub)) {
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void notifySubscriber() {
        for(Subscriber s: subscribers)
            if(s.getItemId() == idItem) {
                s.update(s.getEmail(),content,subject);
            }
    }
}
