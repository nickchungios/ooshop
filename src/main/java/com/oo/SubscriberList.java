package com.oo;

import java.util.ArrayList;
import java.util.List;

public class SubscriberList {
    private static List<Subscriber> marioSubList = new ArrayList();
    private static List<Subscriber> pokemonSubList = new ArrayList<>();
    private static List<Subscriber>  charmanderSubList = new ArrayList<>();
    private static List<Subscriber> luigiSubList = new ArrayList<>();

    private SubscriberList() {


    }

    static {
        marioSubList.add(new Subscriber("Nick","b10523044@yuntech.org.tw",1));
        marioSubList.add(new Subscriber("Nick","zxc87523@gmail.com",1));
//        pokemonSubList.add(new Subscriber("Nick","b10523044@yuntech.org.tw",1));
        pokemonSubList.add(new Subscriber("Nick","zxc87523@gmail.com",1));
    }

    public static List <Subscriber> getMarioSubInstance() {
        return marioSubList;
    }

    public static List <Subscriber> getPokemonInstance() {
        return pokemonSubList;
    }

    public static  void clearMarioList() {
        marioSubList.clear();
    }

    public static  void clearPokemonList() {
        pokemonSubList.clear();
    }

    public static List <Subscriber> getCharmanderSubInstance() {
        return charmanderSubList;
    }

    public static List <Subscriber> getLuigiInstance() {
        return luigiSubList;
    }

    public static  void clearCharmanderList() {
        charmanderSubList.clear();
    }

    public static  void clearLuigiList() {
        luigiSubList.clear();
    }

}
