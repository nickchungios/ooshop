package com.oo;

import java.util.ArrayList;
import java.util.List;

public class AccountList {
    private  static final List<Account> accountList = new ArrayList<>();
    private static long currentUsr = -1;

    private AccountList() {

    }

    static {
        accountList.add(new Account("root@root.com","1234",001));
        accountList.add(new Account("1@1.1","1",002));
    }

    public static List <Account> getAccountList(){
        return accountList;
    }

    public static  void setCurrentUsr(long uid) {
        currentUsr = uid;
    }

    public static long getCurrentUsr(){
        return currentUsr;
    }
}
