package com.oo;

import java.util.concurrent.atomic.AtomicLong;

public class Item {

    private long id;
    private String clas;
    private String name;
    private int quantity;
    private double price;
    private static final AtomicLong counter = new AtomicLong(0);

    public Item(String clas, String name, int quantity, double price, long id) {
        this.clas = clas;
        this.name = name;
        this.quantity = quantity;
        this.price = price;
        this.id = id;
    }

    public Item(String clas, String name, int quantity, double price) {
        this.clas = clas;
        this.name = name;
        this. quantity= quantity;
        this.price = price;
        this.id  = counter.incrementAndGet();
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public String getClas() {
        return clas;
    }

    public int getQuantity() {
        return quantity;
    }
    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
    public double getPrice() {
        return price;
    }

    @Override
    public String toString() {
        return "Item{" + "id=" + id + ", clas=" + clas +
                ", name=" + name + ", quantity="+ quantity +", price=" + price + '}';
    }

}
