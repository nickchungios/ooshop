package com.oo;

import java.io.IOException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(
        name = "ItemServlet",
        urlPatterns = {"/item"}
)

public class ItemServlet extends  HttpServlet{

    ItemService itemService = ItemService.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("searchAction");
        if (action!=null){
            switch (action) {
                case "searchById":
                    searchItemById(req, resp);
                    break;
                case "searchByName":
                    searchItemByName(req, resp);
                    break;
                case "searchItemByNameAndClas":
                    searchItemByNameAndClas(req, resp);
                    break;
                case "getTotalPrice":
                    getTotalPrice(req, resp);
                    break;
            }
        }else{
            List<Item> result = itemService.getAllItem();
            forwardListItem(req, resp, result,"");
        }
    }

    private void searchItemById(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        long idItem = Integer.valueOf(req.getParameter("idItem"));
        Item item = null;
        try {
            item = itemService.getItem(idItem);
        } catch (Exception ex) {
            Logger.getLogger(ItemServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        req.setAttribute("item", item);
        req.setAttribute("action", "edit");
        String nextJSP = "/mario.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        dispatcher.forward(req, resp);
    }

    private void searchItemByName(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String itemName = req.getParameter("itemName");
        List<Item> result = itemService.searchItemByName(itemName);
        forwardListItem(req, resp, result,"");
    }
    private void searchItemByNameAndClas(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String itemName = req.getParameter("itemName");
        String itemClas = req.getParameter("itemClas");
        List<Item> result = itemService.searchItemByNameAndClas(itemName,itemClas);
        forwardListItem(req, resp, result,"");

    }
    private void getTotalPrice(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        double result = itemService.getTotalPrice();
        forwardDoubleItem(req, resp, result);

    }

    private void forwardDoubleItem(HttpServletRequest req, HttpServletResponse resp, double totalPrice)
            throws ServletException, IOException {
        String nextJSP = "/shoppingcart.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        req.setAttribute("totalPrice", totalPrice);
        dispatcher.forward(req, resp);
    }

    private void forwardListItem(HttpServletRequest req, HttpServletResponse resp, List itemList,String nextJSP)
            throws ServletException, IOException {
        if (nextJSP.equals("")) {
            nextJSP = "/shoppingcart.jsp";
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        req.setAttribute("itemList", itemList);
        req.setAttribute("marioList",itemService.getAllMario());
        req.setAttribute("pokemonList",itemService.getAllPokemon());
        req.setAttribute("charmanderList",itemService.getAllCharmander());
        req.setAttribute("luigiList",itemService.getAllLuigi());
        System.out.println(itemList);
        req.setAttribute("undoList", itemService.undoList);
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String action = req.getParameter("action");
        switch (action) {
            case "add":
                addItemAction(req, resp);
                break;
            case "edit":
                editItemAction(req, resp);
                break;
            case "remove":
                removeItemByName(req, resp);
                break;
            case"undo":
                undoItemAction(req,resp);
                break;
            case"goToMarioPage":
                goToMarioPage(req, resp);
                break;
            case"goToManMarioPage":
                goToManMarioPage(req, resp);
                break;
            case"goToPokemonPage":
                goToPokemonPage(req, resp);
                break;
            case"goToManPokemonPage":
                goToManPokemonPage(req, resp);
                break;
            case"goToCharmanderPage":
                goToCharmanderPage(req, resp);
                break;
            case"goToManCharmanderPage":
                goToManCharmanderPage(req, resp);
                break;
            case"goToLuigiPage":
                goToLuigiPage(req, resp);
                break;
            case"goToManLuigiPage":
                goToManLuigiPage(req, resp);
                break;
            case"replenishmentMario":
                replenishmentMarioListAction(req, resp);
                break;
            case "addMarioSub":
                addMarioSub(req, resp);
                break;
            case "removeMarioSub":
                removeMarioSub(req, resp);
                break;
            case"replenishmentPokemon":
                replenishmentPokemonListAction(req, resp);
                break;
            case "addPokemonSub":
                addPokemonSub(req, resp);
                break;
            case "removePokemonSub":
                removePokemonSub(req, resp);
                break;
            case"replenishmentCharmander":
                replenishmentCharmanderListAction(req, resp);
                break;
            case "addCharmanderSub":
                addCharmanderSub(req, resp);
                break;
            case "removeCharmanderSub":
                removeCharmanderSub(req, resp);
                break;
            case"replenishmentLuigi":
                replenishmentLuigiListAction(req, resp);
                break;
            case "addLuigiSub":
                addLuigiSub(req, resp);
                break;
            case "removeLuigiSub":
                removeLuigiSub(req, resp);
                break;
        }

    }

    private void addItemAction(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String clas = req.getParameter("clas");
        String name = req.getParameter("name");
        int quantity = Integer.valueOf(req.getParameter("quantity"));
        double price = Double.parseDouble(req.getParameter("price"));

        Item item;

        List<Item> itemList = itemService.getAllItem();

        long result = itemService.findItemInListByClasAndName(name, clas);


        if (result == -1){
            item = new Item(clas, name, quantity, price);
            long idItem = itemService.addItem(item);
            req.setAttribute("idItem", idItem);
            String message = "The new item has been successfully created.";
            req.setAttribute("message", message);
            forwardListItem(req, resp, itemList,"");
        }  else {

            if("Mario".equals(clas)) {
                System.out.println("testMario");
                for(Item i :itemService.getAllMario()) {
                    if(name.equals(i.getName())) {
                        if (itemService.findItemById(result) == i.getQuantity()) {
                            quantity = i.getQuantity();
                        }else {
                            quantity += itemService.findItemById(result);
                        }
                    }
                }
            }

            if("Pokemon".equals(clas)) {
                System.out.println("testPokemon");
                for (Item i : itemService.getAllPokemon()) {
                    if (name.equals(i.getName())) {
                        System.out.println(i.getQuantity());
                        if (itemService.findItemById(result) == i.getQuantity()) {
                            quantity = i.getQuantity();
                        } else {
                            quantity += itemService.findItemById(result);
                        }
                    }
                }
            }

            if("Charmander".equals(clas)) {
                System.out.println("testCharmander");
                for(Item i :itemService.getAllCharmander()) {
                    if(name.equals(i.getName())) {
                        System.out.println(i.getQuantity());
                        if (itemService.findItemById(result) == i.getQuantity()) {
                            quantity = i.getQuantity();
                        }else {
                            quantity += itemService.findItemById(result);
                        }
                    }
                }
            }

            if("Luigi".equals(clas)) {
                System.out.println("testLuigi");
                for(Item i :itemService.getAllLuigi()) {
                    if(name.equals(i.getName())) {
                        System.out.println(i.getQuantity());
                        if (itemService.findItemById(result) == i.getQuantity()) {
                            quantity = i.getQuantity();
                        }else {
                            quantity += itemService.findItemById(result);
                        }
                    }
                }
            }


            item = new Item(clas, name, quantity, price);
            item.setId(result);
            boolean success = itemService.updateItem(item);
            String message = null;
            if (success) {
                message = "The item has been successfully updated.";
            }
            req.setAttribute("idItem", result);
            req.setAttribute("message", message);
            forwardListItem(req, resp, itemList,"");
        }

    }

    private void editItemAction(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String clas = req.getParameter("clas");
        String name = req.getParameter("name");
        int quantity = Integer.valueOf(req.getParameter("quantity"));
        double price = Double.parseDouble(req.getParameter("price"));
        long idItem = Integer.valueOf(req.getParameter("idItem"));
        Item item = new Item(clas, name, quantity, price);
        item.setId(idItem);
        boolean success = itemService.updateItem(item);
        String message = null;
        if (success) {
            message = "The item has been successfully updated.";
        }
        List<Item> itemList = itemService.getAllItem();
        req.setAttribute("idItem", idItem);
        req.setAttribute("message", message);
        forwardListItem(req, resp, itemList,"");
    }

    private void undoItemAction(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        itemService.undo();
         String message =  "The item has been successfully updated.";
        List<Item> itemList = itemService.getAllItem();
        req.setAttribute("message", message);
        forwardListItem(req, resp, itemList,"");
    }
    private void removeItemByName(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        long idItem = Integer.valueOf(req.getParameter("idItem"));
        boolean confirm = itemService.deleteItem(idItem);
        if (confirm) {
            String message = "The item has been successfully removed.";
            req.setAttribute("message", message);
        }
        List<Item> itemList = itemService.getAllItem();
        forwardListItem(req, resp, itemList,"");
    }

    private void goToMarioPage(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        List<Item> marioList = itemService.getAllMario();
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/mario.jsp");
        req.setAttribute("marioList", marioList);
        System.out.println(itemService.getAllMario());
        dispatcher.forward(req, resp);
    }

    private void goToManMarioPage(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        List<Item> marioList = itemService.getAllMario();
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/manMario.jsp");
        req.setAttribute("marioList", marioList);
        System.out.println(itemService.getAllMario());
        dispatcher.forward(req, resp);
    }

    private void goToPokemonPage(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        List<Item> pokemonList = itemService.getAllPokemon();
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/pokemon.jsp");
        req.setAttribute("pokemonList", pokemonList);
        System.out.println(itemService.getAllPokemon());
        dispatcher.forward(req, resp);
    }

    private void goToManPokemonPage(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        List<Item> pokemonList = itemService.getAllPokemon();
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/manPokemon.jsp");
        req.setAttribute("pokemonList", pokemonList);
        System.out.println(itemService.getAllPokemon());
        dispatcher.forward(req, resp);
    }

    private void goToCharmanderPage(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        List<Item> charmanderList = itemService.getAllCharmander();
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/charmander.jsp");
        req.setAttribute("charmanderList", charmanderList);
        System.out.println(itemService.getAllCharmander());
        dispatcher.forward(req, resp);
    }

    private void goToManCharmanderPage(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        List<Item> charmanderList = itemService.getAllCharmander();
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/manCharmander.jsp");
        req.setAttribute("charmanderList", charmanderList);
        System.out.println(itemService.getAllCharmander());
        dispatcher.forward(req, resp);
    }

    private void goToLuigiPage(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        List<Item> luigiList = itemService.getAllLuigi();
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/luigi.jsp");
        req.setAttribute("luigiList", luigiList);
        System.out.println(itemService.getAllLuigi());
        dispatcher.forward(req, resp);
    }

    private void goToManLuigiPage(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        List<Item> luigiList = itemService.getAllLuigi();
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/manLuigi.jsp");
        req.setAttribute("luigiList", luigiList);
        System.out.println(itemService.getAllLuigi());
        dispatcher.forward(req, resp);
    }

    private void replenishmentMarioListAction(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String clas = req.getParameter("clas");
        String name = req.getParameter("name");
        int quantity = Integer.valueOf(req.getParameter("quantity"));
        double price = Double.parseDouble(req.getParameter("price"));
        long idItem = Integer.valueOf(req.getParameter("idItem"));
        //update phase
        Item item = new Item(clas, name, quantity, price);
        item.setId(idItem);
        boolean success = itemService.updateMario(item);
        String message = null;
        if (success) {
            message = "The item has been successfully updated.";
        }
        List<Item> marioList = itemService.getAllMario();
        req.setAttribute("marioList", marioList);
        System.out.println(marioList);
        req.setAttribute("message", message);

        //remove subscriber and send mail
        String rmMessage = null;

        String mailSub = "Oh!Oh!Shop";
        String mailMsg = name + " is available now!! Come to buy now: www.localhost.com";



        List <Subscriber> marioSubList = itemService.getMarioSubList();
        System.out.println(marioSubList);
        for(Subscriber s: marioSubList)
            if(s.getItemId() == idItem) {
                itemService.sendMail(s.getEmail(), mailMsg, mailSub);
            }

        if(itemService.removeMarioSub(idItem)) {
            rmMessage = "Notify successfully!";
            req.setAttribute("rmMessage", rmMessage);
            System.out.println(rmMessage);
        } else {
            rmMessage = "Notify fail!";
            req.setAttribute("rmMessage", rmMessage);
        }
        req.setAttribute("marioSubList", itemService.getMarioSubList());
        for(Subscriber s:itemService.getMarioSubList()) {
            System.out.println(s.getEmail());
            System.out.println(s.getItemId());
        }
        System.out.println(marioSubList);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
        dispatcher.forward(req, resp);


    }


    private void addMarioSub(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String name = req.getParameter("name");
        String email = req.getParameter("email");
        long idItem = Long.valueOf(req.getParameter("idItem"));

        Subscriber sub = new Subscriber(name,email,idItem);

        String message = null;

        if(itemService.addMarioSub(sub)) {
            message = "Subscribe successfully!";
            req.setAttribute("message", message);
        } else {
            message = "Subscribe fail!";
            req.setAttribute("message", message);
        }
        System.out.println(message);
        req.setAttribute("marioSubList", itemService.getMarioSubList());
        for(Subscriber s:itemService.getMarioSubList()) {
            System.out.println(s.getEmail());
            System.out.println(s.getItemId());
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
        dispatcher.forward(req, resp);
    }

    private void removeMarioSub(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String name = req.getParameter("name");

        long idItem = Long.valueOf(req.getParameter("idItem"));


        String rmMessage = null;

        String mailSub = "Oh!Oh!Shop";
        String mailMsg = name + " is available now!! Come to buy now: www.localhost.com";



        List <Subscriber> marioSubList = itemService.getMarioSubList();
        System.out.println(marioSubList);
        for(Subscriber s: marioSubList)
            if(s.getItemId() == idItem) {
                itemService.sendMail(s.getEmail(), mailMsg, mailSub);
            }

        if(itemService.removeMarioSub(idItem)) {
            rmMessage = "Notify successfully!";
            req.setAttribute("rmMessage", rmMessage);
            System.out.println(rmMessage);
        } else {
            rmMessage = "Notify fail!";
            req.setAttribute("rmMessage", rmMessage);
        }
        req.setAttribute("marioSubList", itemService.getMarioSubList());
        for(Subscriber s:itemService.getMarioSubList()) {
            System.out.println(s.getEmail());
            System.out.println(s.getItemId());
        }
        System.out.println(marioSubList);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
        dispatcher.forward(req, resp);
    }

    private void replenishmentPokemonListAction(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String clas = req.getParameter("clas");
        String name = req.getParameter("name");
        int quantity = Integer.valueOf(req.getParameter("quantity"));
        double price = Double.parseDouble(req.getParameter("price"));
        long idItem = Integer.valueOf(req.getParameter("idItem"));
        //update phase
        Item item = new Item(clas, name, quantity, price);
        item.setId(idItem);
        boolean success = itemService.updatePokemon(item);
        String message = null;
        if (success) {
            message = "The item has been successfully updated.";
        }
        List<Item> pokemonList = itemService.getAllPokemon();
        req.setAttribute("pokemonList", pokemonList);
        System.out.println(pokemonList);
        req.setAttribute("message", message);

        //remove subscriber and send mail
        String rmMessage = null;

        String mailSub = "Oh!Oh!Shop";
        String mailMsg = name + " is available now!! Come to buy now: www.localhost.com";



        List <Subscriber> pokemonSubList = itemService.getPokemonSubList();
        System.out.println(pokemonSubList);
        for(Subscriber s: pokemonSubList)
            if(s.getItemId() == idItem) {
                itemService.sendMail(s.getEmail(), mailMsg, mailSub);
            }

        if(itemService.removePokemonSub(idItem)) {
            rmMessage = "Notify successfully!";
            req.setAttribute("rmMessage", rmMessage);
            System.out.println(rmMessage);
        } else {
            rmMessage = "Notify fail!";
            req.setAttribute("rmMessage", rmMessage);
        }
        req.setAttribute("pokemonSubList", itemService.getPokemonSubList());
        for(Subscriber s:itemService.getPokemonSubList()) {
            System.out.println(s.getEmail());
            System.out.println(s.getItemId());
        }
        System.out.println(pokemonSubList);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
        dispatcher.forward(req, resp);


    }


    private void addPokemonSub(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String name = req.getParameter("name");
        String email = req.getParameter("email");
        long idItem = Long.valueOf(req.getParameter("idItem"));

        Subscriber sub = new Subscriber(name,email,idItem);

        String message = null;

        if(itemService.addPokemonSub(sub)) {
            message = "Subscribe successfully!";
            req.setAttribute("message", message);
        } else {
            message = "Subscribe fail!";
            req.setAttribute("message", message);
        }
        System.out.println(message);
        req.setAttribute("marioSubList", itemService.getPokemonSubList());
        for(Subscriber s:itemService.getPokemonSubList()) {
            System.out.println(s.getEmail());
            System.out.println(s.getItemId());
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
        dispatcher.forward(req, resp);
    }

    private void removePokemonSub(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String name = req.getParameter("name");

        long idItem = Long.valueOf(req.getParameter("idItem"));


        String rmMessage = null;

        String mailSub = "Oh!Oh!Shop";
        String mailMsg = name + " is available now!! Come to buy now: www.localhost.com";



        List <Subscriber> pokemonSubList = itemService.getPokemonSubList();
        System.out.println(pokemonSubList);
        for(Subscriber s: pokemonSubList)
            if(s.getItemId() == idItem) {
                itemService.sendMail(s.getEmail(), mailMsg, mailSub);
            }

        if(itemService.removePokemonSub(idItem)) {
            rmMessage = "Notify successfully!";
            req.setAttribute("rmMessage", rmMessage);
            System.out.println(rmMessage);
        } else {
            rmMessage = "Notify fail!";
            req.setAttribute("rmMessage", rmMessage);
        }
        req.setAttribute("pokemonSubList", itemService.getPokemonSubList());
        for(Subscriber s:itemService.getPokemonSubList()) {
            System.out.println(s.getEmail());
            System.out.println(s.getItemId());
        }
        System.out.println(pokemonSubList);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
        dispatcher.forward(req, resp);
    }

    private void replenishmentCharmanderListAction(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String clas = req.getParameter("clas");
        String name = req.getParameter("name");
        int quantity = Integer.valueOf(req.getParameter("quantity"));
        double price = Double.parseDouble(req.getParameter("price"));
        long idItem = Integer.valueOf(req.getParameter("idItem"));
        //update phase
        Item item = new Item(clas, name, quantity, price);
        item.setId(idItem);
        boolean success = itemService.updateCharmander(item);
        String message = null;
        if (success) {
            message = "The item has been successfully updated.";
        }
        List<Item> charmanderList = itemService.getAllCharmander();
        req.setAttribute("charmanderList", charmanderList);
        System.out.println(charmanderList);
        req.setAttribute("message", message);

        //remove subscriber and send mail
        String rmMessage = null;

        String mailSub = "Oh!Oh!Shop";
        String mailMsg = name + " is available now!! Come to buy now: www.localhost.com";



        List <Subscriber> charmanderSubList = itemService.getCharmanderSubList();
        System.out.println(charmanderSubList);
        for(Subscriber s: charmanderSubList)
            if(s.getItemId() == idItem) {
                itemService.sendMail(s.getEmail(), mailMsg, mailSub);
            }

        if(itemService.removeCharmanderSub(idItem)) {
            rmMessage = "Notify successfully!";
            req.setAttribute("rmMessage", rmMessage);
            System.out.println(rmMessage);
        } else {
            rmMessage = "Notify fail!";
            req.setAttribute("rmMessage", rmMessage);
        }
        req.setAttribute("charmanderSubList", itemService.getCharmanderSubList());
        for(Subscriber s:itemService.getCharmanderSubList()) {
            System.out.println(s.getEmail());
            System.out.println(s.getItemId());
        }
        System.out.println(charmanderSubList);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
        dispatcher.forward(req, resp);


    }


    private void addCharmanderSub(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String name = req.getParameter("name");
        String email = req.getParameter("email");
        long idItem = Long.valueOf(req.getParameter("idItem"));

        Subscriber sub = new Subscriber(name,email,idItem);

        String message = null;

        if(itemService.addCharmanderSub(sub)) {
            message = "Subscribe successfully!";
            req.setAttribute("message", message);
        } else {
            message = "Subscribe fail!";
            req.setAttribute("message", message);
        }
        System.out.println(message);
        req.setAttribute("charmanderSubList", itemService.getCharmanderSubList());
        for(Subscriber s:itemService.getCharmanderSubList()) {
            System.out.println(s.getEmail());
            System.out.println(s.getItemId());
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
        dispatcher.forward(req, resp);
    }

    private void removeCharmanderSub(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String name = req.getParameter("name");

        long idItem = Long.valueOf(req.getParameter("idItem"));


        String rmMessage = null;

        String mailSub = "Oh!Oh!Shop";
        String mailMsg = name + " is available now!! Come to buy now: www.localhost.com";



        List <Subscriber> charmanderSubList = itemService.getCharmanderSubList();
        System.out.println(charmanderSubList);
        for(Subscriber s: charmanderSubList)
            if(s.getItemId() == idItem) {
                itemService.sendMail(s.getEmail(), mailMsg, mailSub);
            }

        if(itemService.removeCharmanderSub(idItem)) {
            rmMessage = "Notify successfully!";
            req.setAttribute("rmMessage", rmMessage);
            System.out.println(rmMessage);
        } else {
            rmMessage = "Notify fail!";
            req.setAttribute("rmMessage", rmMessage);
        }
        req.setAttribute("charmanderSubList", itemService.getCharmanderSubList());
        for(Subscriber s:itemService.getCharmanderSubList()) {
            System.out.println(s.getEmail());
            System.out.println(s.getItemId());
        }
        System.out.println(charmanderSubList);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
        dispatcher.forward(req, resp);
    }


    private void replenishmentLuigiListAction(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String clas = req.getParameter("clas");
        String name = req.getParameter("name");
        int quantity = Integer.valueOf(req.getParameter("quantity"));
        double price = Double.parseDouble(req.getParameter("price"));
        long idItem = Integer.valueOf(req.getParameter("idItem"));
        //update phase
        Item item = new Item(clas, name, quantity, price);
        item.setId(idItem);
        boolean success = itemService.updateLuigi(item);
        String message = null;
        if (success) {
            message = "The item has been successfully updated.";
        }
        List<Item> luigiList = itemService.getAllLuigi();
        req.setAttribute("luigiList", luigiList);
        System.out.println(luigiList);
        req.setAttribute("message", message);

        //remove subscriber and send mail
        String rmMessage = null;

        String mailSub = "Oh!Oh!Shop";
        String mailMsg = name + " is available now!! Come to buy now: www.localhost.com";



        List <Subscriber> luigiSubList = itemService.getLuigiSubList();
        System.out.println(luigiSubList);
        for(Subscriber s: luigiSubList)
            if(s.getItemId() == idItem) {
                itemService.sendMail(s.getEmail(), mailMsg, mailSub);
            }

        if(itemService.removeLuigiSub(idItem)) {
            rmMessage = "Notify successfully!";
            req.setAttribute("rmMessage", rmMessage);
            System.out.println(rmMessage);
        } else {
            rmMessage = "Notify fail!";
            req.setAttribute("rmMessage", rmMessage);
        }
        req.setAttribute("luigiSubList", itemService.getLuigiSubList());
        for(Subscriber s:itemService.getLuigiSubList()) {
            System.out.println(s.getEmail());
            System.out.println(s.getItemId());
        }
        System.out.println(luigiSubList);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
        dispatcher.forward(req, resp);


    }


    private void addLuigiSub(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String name = req.getParameter("name");
        String email = req.getParameter("email");
        long idItem = Long.valueOf(req.getParameter("idItem"));

        Subscriber sub = new Subscriber(name,email,idItem);

        String message = null;

        if(itemService.addLuigiSub(sub)) {
            message = "Subscribe successfully!";
            req.setAttribute("message", message);
        } else {
            message = "Subscribe fail!";
            req.setAttribute("message", message);
        }
        System.out.println(message);
        req.setAttribute("luigiSubList", itemService.getLuigiSubList());
        for(Subscriber s:itemService.getLuigiSubList()) {
            System.out.println(s.getEmail());
            System.out.println(s.getItemId());
        }
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
        dispatcher.forward(req, resp);
    }

    private void removeLuigiSub(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        String name = req.getParameter("name");

        long idItem = Long.valueOf(req.getParameter("idItem"));


        String rmMessage = null;

        String mailSub = "Oh!Oh!Shop";
        String mailMsg = name + " is available now!! Come to buy now: www.localhost.com";



        List <Subscriber> luigiSubList = itemService.getLuigiSubList();
        System.out.println(luigiSubList);
        for(Subscriber s: luigiSubList)
            if(s.getItemId() == idItem) {
                itemService.sendMail(s.getEmail(), mailMsg, mailSub);
            }

        if(itemService.removeLuigiSub(idItem)) {
            rmMessage = "Notify successfully!";
            req.setAttribute("rmMessage", rmMessage);
            System.out.println(rmMessage);
        } else {
            rmMessage = "Notify fail!";
            req.setAttribute("rmMessage", rmMessage);
        }
        req.setAttribute("luigiSubList", itemService.getLuigiSubList());
        for(Subscriber s:itemService.getLuigiSubList()) {
            System.out.println(s.getEmail());
            System.out.println(s.getItemId());
        }
        System.out.println(luigiSubList);
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/success.jsp");
        dispatcher.forward(req, resp);
    }



}