package com.oo;

import java.util.concurrent.atomic.AtomicLong;

public class Account {
    private long uid;
    private String account;
    private String password;
    private static final AtomicLong counter = new AtomicLong(10001);

    public Account(String account, String password, long uid) {
        this.account = account;
        this.password = password;
        this.uid = uid;
    }

    public  Account(String account, String password) {
        this.account = account;
        this.password = password;
        this.uid  = counter.incrementAndGet();
    }

    public long getUid() {
        return uid;
    }
    public void setUid(long uid) {
        this.uid = uid;
    }
    public String getAccount() {
        return account;
    }
    public String getPassword() {
        return password;
    }

    @Override
    public String toString() {
        return "Account{" + "uid=" + uid + ", account=" + account +
                ", password=" + password + '}';
    }
}
