package com.oo;

import java.util.concurrent.atomic.AtomicLong;

public class Payment {

   private String firstName;
   private String lastName;
   private String email;
   private String phone;
   private String address;
   private String pay;
   private String cardNumber;
   private String validMonth;
   private String validYear;
   private String cyc;

    public Payment(String firstName, String lastName, String email, String phone, String address, String pay) {
            this.firstName = firstName;
            this.lastName = lastName;
            this.email = email;
            this.phone = phone;
            this.address = address;
            this.pay = pay;
    }

    public Payment(String firstName, String lastName, String email, String phone, String address, String pay, String cardNumber,  String validMonth, String validYear, String cyc) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.phone = phone;
        this.address = address;
        this.pay = pay;
        this.cardNumber = cardNumber;
        this.validMonth = validMonth;
        this.validYear = validYear;
        this.cyc = cyc;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getEmail() {
        return email;
    }

    public String getPhone() {
        return phone;
    }

    public String getAddress() {
        return address;
    }

    public String getPay() {
        return pay;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public String getValidMonth() {
        return validMonth;
    }

    public String getValidYear() {
        return validYear;
    }

    public String getCyc() {
        return cyc;
    }



    @Override
    public String toString() {
        return "Payment{" + "firstName=" + firstName + ", lastName=" + lastName +
                ", email=" + email + ", address="+ address +", pay=" + pay + '}';
    }
}
