package com.oo;


import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

@WebServlet(
        name = "OrderServlet",
        urlPatterns = {"/order"}
)
public class OrderServlet extends HttpServlet {

    OrderService orderService = OrderService.getInstance();
    ItemService itemService = ItemService.getInstance();
    AccountService accountService = AccountService.getInstance();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String action = req.getParameter("searchAction");
        if (action!=null){
            switch (action) {
                case "searchById":
                    searchOrderById(req, resp);
                    break;
                case "searchByAccount":
                    searchOrderByAccount(req, resp);
                    break;
                case "searchOrderByDate":
                    searchOrderByDate(req, resp);
                    break;
                case  "searchOrderByCurrentUser":
                    searchOrderByCurrentUser(req, resp);
            }
        }else{
            List<Order> result = orderService.getAllOrder();
            forwardListItem(req, resp, result);
        }
    }

    private void searchOrderById(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        long idOrder = Integer.valueOf(req.getParameter("idOrder"));
        Order order = null;
        try {
            order = orderService.getOrder(idOrder);
        } catch (Exception ex) {
            Logger.getLogger(OrderServlet.class.getName()).log(Level.SEVERE, null, ex);
        }
        req.setAttribute("order", order);
        req.setAttribute("action", "edit");
        String nextJSP = "/orderDetail.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        dispatcher.forward(req, resp);
    }

    private void searchOrderByAccount(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String account = req.getParameter("account");
        List<Order> result = orderService.searchOrderByAccount(account);
        forwardListItem(req, resp, result);
    }
    private void searchOrderByDate(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String orderDate = req.getParameter("orederDate");
        List<Order> result = orderService.searchOrderByDate(orderDate);
        forwardListItem(req, resp, result);

    }
    private void searchOrderByCurrentUser(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {

        long currentUser = accountService.getCrrentUser();
        Account account;
        String email;
        if(currentUser != -1 ) {
            try {
                account = accountService.getAccount(currentUser);
                email = account.getAccount();
                List<Order> result = orderService.searchOrderByAccount(email);
                List<Order> orderList = orderService.getAllOrder();
                forwardListItem(req, resp, orderList);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            String nextJSP = "/login.jsp";
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
            dispatcher.forward(req, resp);
        }

    }


    private void forwardListItem(HttpServletRequest req, HttpServletResponse resp, List<Order> orderList)
            throws ServletException, IOException {
        String nextJSP = "/orderDetail.jsp";
        RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
        req.setAttribute("orderList", orderList);
        req.setAttribute("marioList", itemService.getAllMario());
        req.setAttribute("pokemonList", itemService.getAllPokemon());
        req.setAttribute("charmanderList", itemService.getAllCharmander());
        req.setAttribute("luigiList", itemService.getAllLuigi());
        List<Order> currentUserOrder = new ArrayList<>();
        long currentUser = accountService.getCrrentUser();
        Account currentUserAccount = null;
        try {
            currentUserAccount = accountService.getAccount(currentUser);
        } catch (Exception e) {
            e.printStackTrace();
        }
        for(Order o: orderList) {
            assert currentUserAccount != null;
            if (o.getAccount().equals(currentUserAccount.getAccount())){
                currentUserOrder.add(o);
            }
        }
        req.setAttribute("currentUserOrder", currentUserOrder);
        dispatcher.forward(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String action = req.getParameter("action");
        switch (action) {
            case "add":
                addOrderAction(req, resp);
                break;
            case "edit":
                editOrderAction(req, resp);
                break;
            case "remove":
                removeOrderById(req, resp);
                break;
        }

    }

    private void addOrderAction(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        String date  = new SimpleDateFormat("yyyyMMdd_HHmmss").format(Calendar.getInstance().getTime());
        double totalPrice = itemService.getTotalPrice();
        long currentUser = accountService.getCrrentUser();
        String email = "";
        try {
            Account account = accountService.getAccount(currentUser);
            email = account.getAccount();
        } catch (Exception e) {
            e.printStackTrace();
        }

        String firstName = req.getParameter("firstName");
        String lastName = req.getParameter("lastName");
        String paymentEmail = req.getParameter("email");
        String phone = req.getParameter("phone");
        String address = req.getParameter("address");
        String pay = req.getParameter("pay");
        String cardNumber = req.getParameter("cardNumber");
        String validMonth = req.getParameter("month");
        String validYear = req.getParameter("year");
        String cyc = req.getParameter("cyc");

        Payment payment;

        if ( pay.equals("COD")) {
            payment = new Payment(firstName,lastName,paymentEmail,phone,address,pay);
        } else {
            payment = new Payment(firstName,lastName,paymentEmail,phone,address,pay,cardNumber,validMonth,validYear,cyc);
        }

        Order order;

        List<Item> itemList = itemService.getAllItem();

        List<Item> currentOrderList = new ArrayList();

        String[] nationalArray = {"1/01","2/15","2/16","2/17","2/18","2/18","4/04","4/05","6/18","9/24","10/10"};
        SimpleDateFormat sdf = new SimpleDateFormat("MM/dd");
        Date now = new Date();

        String currentDate = sdf.format(now);
        System.out.println(currentDate);
        boolean isHoliday = false;


        for(int i = 0;i<nationalArray.length;i++) {
            if(nationalArray[i].equals(currentDate)) {
                isHoliday = true;
            }
        }

        int cnt = 0;
        for(Item i: itemList) {
            cnt++;
            currentOrderList.add(i);
            if(i.getQuantity() >= 100) {
                totalPrice = totalPrice - (i.getPrice() * i.getQuantity()) + (i.getPrice() * i.getQuantity() * 0.85);
            }
            if(isHoliday) {
                if (i.getQuantity() >= 2) {
                    totalPrice = totalPrice - i.getPrice();
                }
            }
        }

        double lastYearCnt = 0;
        for(Order o :orderService.getAllOrder()) {
            if (o.getAccount().equals(email)) {
                lastYearCnt += o.getTotalPrice();
            }

        }
        if (lastYearCnt >= 100000){
            totalPrice = totalPrice * 0.8;
        }
        if(cnt >= 3) {
            totalPrice = totalPrice * 0.95;
        }





        List<Order> orderList;


        if (currentUser == -1){
            String nextJSP = "/login.jsp";
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
            dispatcher.forward(req, resp);
        }  else {
                boolean success = true;
                for(Item i:currentOrderList) {
                    if("Mario".equals(i.getClas())) {
                        for (Item ii : itemService.getAllMario()) {
                            if ((ii.getName()).equals(i.getName())) {
                                if ((ii.getQuantity() - i.getQuantity()) < 0) {
                                    success = false;
                                }
                            }
                        }
                    }
                    if("Pokemon".equals(i.getClas())) {
                        for (Item ii : itemService.getAllPokemon()) {
                            if ((ii.getName()).equals(i.getName())) {
                                if ((ii.getQuantity() - i.getQuantity()) < 0) {
                                    success = false;
                                }
                            }
                        }
                    }
                    if("Charmander".equals(i.getClas())) {
                        for(Item ii: itemService.getAllCharmander()) {
                            if ((ii.getName()).equals(i.getName())) {
                                if((ii.getQuantity() - i.getQuantity()) < 0 ){
                                    success = false;
                                }
                            }
                        }
                    }
                    if("Luigi".equals(i.getClas())) {
                        for(Item ii: itemService.getAllLuigi()) {
                            if ((ii.getName()).equals(i.getName())) {
                                if((ii.getQuantity() - i.getQuantity()) < 0 ){
                                    success = false;
                                }
                            }
                        }
                    }
                }

            if(success) {
                for(Item i:currentOrderList) {
                    if("Mario".equals(i.getClas())) {
                        for (Item ii : itemService.getAllMario()) {
                            if ((ii.getName()).equals(i.getName())) {
                                 ii.setQuantity(ii.getQuantity() - i.getQuantity());
                            }
                        }
                    }
                    if("Pokemon".equals(i.getClas())) {
                        for(Item ii: itemService.getAllPokemon()) {
                            if ((ii.getName()).equals(i.getName())) {
                                ii.setQuantity(ii.getQuantity() - i.getQuantity());
                            }
                        }
                    }
                    if("Charmander".equals(i.getClas())) {
                        for(Item ii: itemService.getAllCharmander()) {
                            if ((ii.getName()).equals(i.getName())) {
                                ii.setQuantity(ii.getQuantity() - i.getQuantity());
                            }
                        }
                    }

                    if("Luigi".equals(i.getClas())) {
                        for(Item ii: itemService.getAllLuigi()) {
                            if ((ii.getName()).equals(i.getName())) {
                                ii.setQuantity(ii.getQuantity() - i.getQuantity());
                            }
                        }
                    }
                }

                order = new Order(date, email, currentOrderList, totalPrice, payment);
                long idOrder = orderService.addOrder(order);
                req.setAttribute("idOrder", idOrder);
                String message = "The new Order has been successfully created.";
                req.setAttribute("message", message);
                orderList = orderService.getAllOrder();
                System.out.println(orderService.getAllOrder());
                forwardListItem(req, resp, orderList);

                itemService.clearItemList();
                itemService.clearUndoList();
            }else { String message = "Create fail.";
                req.setAttribute("message", message);
                orderList = orderService.getAllOrder();
                System.out.println(message);
                String nextJSP = "/itemSoldOut.jsp";
                RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
                dispatcher.forward(req, resp);

            }
        }

    }

    private void editOrderAction(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
//        String clas = req.getParameter("clas");
//        String name = req.getParameter("name");
//        int quantity = Integer.valueOf(req.getParameter("quantity"));
//        double price = Double.parseDouble(req.getParameter("price"));
//        long idItem = Integer.valueOf(req.getParameter("idItem"));
//        Item item = new Item(clas, name, quantity, price);
//        item.setId(idItem);
//        boolean success = itemService.updateItem(item);
//        String message = null;
//        if (success) {
//            message = "The item has been successfully updated.";
//        }
//        List<Item> itemList = itemService.getAllItem();
//        req.setAttribute("idItem", idItem);
//        req.setAttribute("message", message);
//        forwardListItem(req, resp, itemList);
    }

    private void removeOrderById(HttpServletRequest req, HttpServletResponse resp)
            throws ServletException, IOException {
        long idOrder = Integer.valueOf(req.getParameter("idOrder"));

        List<Item> itemList = new ArrayList<>();

        for(Order i: orderService.getAllOrder()) {
            if(i.getId() == idOrder) {
                itemList = i.getItemList();
            }
        }

        boolean confirm = orderService.deleteOrder(idOrder);

        if (confirm){
            for(Item i: itemList) {
                if("Mario".equals(i.getClas())) {
                    for (Item ii : itemService.getAllMario()) {
                        if ((ii.getName()).equals(i.getName())) {
                            ii.setQuantity(ii.getQuantity() + i.getQuantity());
                        }
                    }
                }
                if("Pokemon".equals(i.getClas())) {
                    for(Item ii: itemService.getAllPokemon()) {
                        if ((ii.getName()).equals(i.getName())) {
                            ii.setQuantity(ii.getQuantity() + i.getQuantity());
                        }
                    }
                }
            }
            String message = "The order has been successfully removed.";
            System.out.println(message);
            req.setAttribute("message", message);
            List<Order> orderList = orderService.getAllOrder();
            System.out.println(orderList);
            String nextJSP = "/success.jsp";
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(nextJSP);
            dispatcher.forward(req, resp);
        }
    }
}
